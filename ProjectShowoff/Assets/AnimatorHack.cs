using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorHack : MonoBehaviour
{
	[SerializeField] private Animator animator;


	private void Awake()
	{
		animator.updateMode = AnimatorUpdateMode.UnscaledTime;
	}
}
