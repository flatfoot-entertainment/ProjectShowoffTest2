using UnityEngine;
using UnityEngine.UI;

public class VCAController : MonoBehaviour
{
    private FMOD.Studio.VCA VcaController;
    public string VcaName;

    private void Start()
    {
        VcaController = FMODUnity.RuntimeManager.GetVCA("vca:/" + VcaName);
    }

    public void SetVolume(float volume)
    {
        VcaController.setVolume(volume);
        if (VcaName.Equals("SFX"))
        {
            SoundManager.Instance.SfxVolume = volume;
        }
        else if (VcaName.Equals("music"))
        {
            SoundManager.Instance.MusicVolume = volume;
        }
    }



}
