using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BoxScript<Contained> : MonoBehaviour
{
	public abstract Contained contained { get; set; }

	private void Start()
	{
		OnStart();
	}

	protected virtual void OnStart() { }

	public virtual void OnAddedToBox() { }

	public virtual void OnRemovedFromBox() { }

	private void OnDrawGizmos()
	{
		if (Application.isPlaying)
		{
			RaycastHit hit;
			if (Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity))
			{
				Gizmos.DrawLine(transform.position, new Vector3(transform.position.x, hit.transform.position.y, transform.position.z));
			}
		}
	}
}
