using UnityEngine;

public class ItemScript : BoxScript<Item>
{
	public override Item contained { get; set; }

	public bool CanShake;

	public override void OnAddedToBox()
	{
		GetComponent<ItemOutline>().RemoveOutline();
		CanShake = false;
	}

	public override void OnRemovedFromBox()
	{
		CanShake = true;
	}
}