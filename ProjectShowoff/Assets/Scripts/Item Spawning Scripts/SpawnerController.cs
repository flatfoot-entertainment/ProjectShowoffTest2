using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
	public int ConveyorInitialSpeed = 2;
	public float SpawnInterval
	{
		get => spawnInterval;
		set => spawnInterval = value;
	}

	[SerializeField] private List<ItemSpawner> spawners = new List<ItemSpawner>();
	[SerializeField] private List<ConveyorSetupScript> conveyors = new List<ConveyorSetupScript>();
	[SerializeField] private List<ConveyorSetupScript> leftConveyors = new List<ConveyorSetupScript>();
	[SerializeField] private List<ConveyorSetupScript> rightConveyors = new List<ConveyorSetupScript>();
	[SerializeField] private SimpleConveyor boxConveyor; // This one also has to be stopped

	[SerializeField] private float spawnInterval;
	public float InitialSpawnInterval { get; private set; }

	private void Start()
	{
		spawnInterval = ConfigLoader.GameConfig?.ItemSpawnSettings?.SpawnTime ?? spawnInterval;
		InitialSpawnInterval = spawnInterval;
	}

	[SerializeField] private float conveyorDelay;

	public IEnumerator Coroutine()
	{
		while (gameObject)
		{
			Spawn();
			yield return new WaitForSeconds(SpawnInterval);
		}
	}

	private void Spawn()
	{
		spawners.ForEach(s => s.Spawn());
	}

	public void StopLeftConveyors()
	{
		StopConveyorCollection(leftConveyors);
	}

	public void StopRightConveyors()
	{
		StopConveyorCollection(rightConveyors);
	}

	private void StopConveyorCollection(List<ConveyorSetupScript> collection)
	{
		foreach (var conv in collection)
		{
			foreach (SimpleConveyor conveyorPart in conv.ConveyorScripts)
			{
				if (!conveyorPart.IsStopped)
				{
					StartCoroutine(conveyorPart.Stop(conveyorDelay));
				}
			}
			foreach (ItemSpawner spawner in conv.ItemSpawners)
			{
				if (spawner.CanSpawn)
				{
					StartCoroutine(spawner.Stop(conveyorDelay));
				}
			}

			foreach (var animator in conv.Animators)
			{
				StartCoroutine(animator.Stop(conveyorDelay));
			}
		}
	}

	public void AddSpawner(ItemSpawner spawner)
	{
		spawners.Add(spawner);
	}

	public void AddSpawners(ItemSpawner[] pSpawners)
	{
		foreach (ItemSpawner pSpawner in pSpawners)
		{
			AddSpawner(pSpawner);
		}
	}

	public void RemoveSpawner(ItemSpawner spawner)
	{
		spawners.Remove(spawner);
	}

	public void RemoveSpawnerAt(int index)
	{
		if (index < 0 && index >= spawners.Count)
		{
			return;
		}
		spawners.RemoveAt(index);
	}

	public void AddConveyor(ConveyorSetupScript conveyor)
	{
		conveyors.Add(conveyor);
	}

	public void RemoveConveyor(ConveyorSetupScript conveyor)
	{
		conveyors.Remove(conveyor);
	}

	public void RemoveConveyorAt(int index)
	{
		if (index < 0 && index >= conveyors.Count)
		{
			return;
		}
		conveyors.RemoveAt(index);
	}

	public void ChangeConveyorSpeed(float speed)
	{
		foreach (ConveyorSetupScript conveyor in conveyors)
		{
			foreach (SimpleConveyor simpleConveyor in conveyor.ConveyorScripts)
			{
				simpleConveyor.Speed = speed;
			}
		}

		if (boxConveyor) boxConveyor.Speed = speed;
	}
}
