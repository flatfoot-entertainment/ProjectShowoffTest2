﻿using UnityEngine;

public class ExplosiveItemPropertyHandler : ItemPropertyHandler
{
	public float radius;
	public float force;
	/// <inheritdoc />
	protected override void OnDestruction()
	{
		// OverlapSphere shouldn't make a difference to OverlapSphereNonAlloc (maybe even better)
		// -> We only allocate this once anyway AND we want all in the sphere.
		var colliders = Physics.OverlapSphere(transform.position, radius);
		foreach (Collider coll in colliders)
		{
			var rb = coll.GetComponent<Rigidbody>();
			var itemScript = coll.GetComponent<ItemScript>();
			if (itemScript && itemScript.CanShake && rb)
			{
				rb.AddExplosionForce(force, transform.position, radius);
			}
		}
		base.OnDestruction();
	}
}
