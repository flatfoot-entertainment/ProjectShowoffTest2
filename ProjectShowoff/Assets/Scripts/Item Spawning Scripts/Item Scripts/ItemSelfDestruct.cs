﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class ItemSelfDestruct : MonoBehaviour
{
	[SerializeField] private float selfDestructTime;
	[SerializeField] private float timeItStartsBlinking;
	[SerializeField] private LayerMask floorLayerMask;
	private float currentTimer;

	private Rigidbody rb;
	private float oldSleepThreshold;

	private GameObject itemModel;

#if UNITY_EDITOR
	private bool countingDown;
#endif

	private Coroutine blinkCoroutine;

	private void Start()
	{
		selfDestructTime = 5f;
		timeItStartsBlinking = 2f;
		itemModel = transform.GetChild(0).gameObject;
		currentTimer = selfDestructTime;
		rb = GetComponent<Rigidbody>();

	}

	private void OnCollisionEnter(Collision other)
	{
		if (floorLayerMask.Contains(other.gameObject.layer))
		{
			blinkCoroutine = null;
			oldSleepThreshold = rb.sleepThreshold;
			rb.sleepThreshold = 0f;

#if UNITY_EDITOR
			countingDown = true;
#endif
		}
	}

	private void OnCollisionStay(Collision other)
	{
		if (!floorLayerMask.Contains(other.gameObject.layer)) return;
		currentTimer -= Time.fixedDeltaTime;
		if (currentTimer <= timeItStartsBlinking)
		{
			if (blinkCoroutine == null) blinkCoroutine = StartCoroutine(BlinkItem());
			if (currentTimer <= 0f)
			{
				Destroy(gameObject);
			}
		}

	}

	private IEnumerator BlinkItem()
	{
		while (gameObject)
		{
			yield return new WaitForSeconds(0.25f);
			itemModel.SetActive(false);
			yield return new WaitForSeconds(0.25f);
			itemModel.SetActive(true);

		}
	}

	private void OnCollisionExit(Collision other)
	{
		if (floorLayerMask.Contains(other.gameObject.layer))
		{
			currentTimer = selfDestructTime;
			rb.sleepThreshold = oldSleepThreshold;
			if (blinkCoroutine != null)
			{
				StopCoroutine(blinkCoroutine);
			}
			blinkCoroutine = null;
#if UNITY_EDITOR
			countingDown = false;

#endif
		}
	}

#if UNITY_EDITOR
	private void OnDrawGizmos()
	{
		if (countingDown)
		{
			UnityEditor.Handles.color = Color.red;
			Gizmos.color = Color.red;
			Gizmos.DrawRay(transform.position, Vector3.up);
			UnityEditor.Handles.Label(transform.position + Vector3.up, $"{Mathf.RoundToInt(currentTimer)}s");
		}
	}
#endif
}
