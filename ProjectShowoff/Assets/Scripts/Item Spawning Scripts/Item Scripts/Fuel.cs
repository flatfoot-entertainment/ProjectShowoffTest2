using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuel : Item
{
	public Fuel(int pPrice) : base(ItemType.Fuel, pPrice) { }
}
