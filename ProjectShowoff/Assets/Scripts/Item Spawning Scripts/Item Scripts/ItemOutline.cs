﻿using System.Collections.Generic;
using UnityEngine;

public class ItemOutline : MonoBehaviour
{
	public bool IsHooked
	{
		get => isHooked;
		set => isHooked = value;
	}
	[SerializeField] private Outline.Mode outlineMode;
	[SerializeField] private Color outlineColor;
	[SerializeField] private float outlineWidth;

	[SerializeField] private bool isHooked;

	private List<Outline> outlines = new List<Outline>();

	private void OnMouseEnter()
	{
		MakeOutline();
	}

	private void OnMouseExit()
	{
		if (isHooked) return;
		RemoveOutline();
	}

	public void RemoveOutline()
	{
		foreach (var outline in outlines) Destroy(outline);
		outlines.Clear();
	}

	public void MakeOutline()
	{
		foreach (var child in GetComponentsInChildren<Renderer>())
		{
			var outline = child.gameObject.AddComponent<Outline>();
			if (outline == null) continue;
			outline.OutlineMode = outlineMode;
			outline.OutlineColor = outlineColor;
			outline.OutlineWidth = outlineWidth;
			outlines.Add(outline);
		}
	}
}
