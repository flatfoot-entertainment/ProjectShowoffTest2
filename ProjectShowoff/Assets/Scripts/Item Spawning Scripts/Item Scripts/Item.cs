using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The different types of items
/// </summary>
public enum ItemType
{
	Food,
	MechanicalParts,
	Medicine,
	Fuel
}

public class Item
{
	/// <summary>
	/// The type of the item
	/// </summary>
	public ItemType Type { get; }

	/// <summary>
	/// The value of the item. Because of legacy reasons, this is always 1
	/// </summary>
	public static int Value => 1;
	
	/// <summary>
	/// The price of the item
	/// </summary>
	public int Price { get; }

	protected Item(ItemType pType, int pPrice)
	{
		Type = pType;
		Price = pPrice;
	}
}
