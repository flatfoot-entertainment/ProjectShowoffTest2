﻿using UnityEngine;

public class ItemPropertyHandler : MonoBehaviour
{
	public GameObject replacedBy;
	public float maxForce;

	private void OnCollisionEnter(Collision collision)
	{
		// If the force is to high, replace the object by the specified one
		if (collision.relativeVelocity.magnitude < maxForce) return;
		Lean.Pool.LeanPool.Despawn(gameObject);
		if (replacedBy)
			Lean.Pool.LeanPool.Spawn(replacedBy, transform.position, transform.rotation);
		OnDestruction();
	}
	
	/// <summary>
	/// Override this to get a callback when the item is destructed
	/// </summary>
	protected virtual void OnDestruction() { }
}
