using UnityEngine;

public class PercentageItemFactory : MonoBehaviour
{
	[PlayModeReadOnly, SerializeField] private ItemSpawningSettings settings;
	private ItemSpawningSettings settingsCopy;
	public int ItemSpawnLayer => settingsCopy.ItemSpawnLayer;
	public int ItemPrice => settingsCopy.ItemValue;

	private void Awake()
	{
		settingsCopy = Instantiate(settings);
	}

	/// <summary>
	/// Get a random item
	/// </summary>
	public GameObject CreateRandomItem() => settingsCopy.GetRandom();

	/// <summary>
	/// Get a random type, also affected by the percentages
	/// </summary>
	/// <returns></returns>
	public ItemType GetRandomType() => settingsCopy.GetRandomType();

	/// <summary>
	/// Apply restrictions to which items can be spawned using <see cref="ProgressionSettings.ItemFlags"/>
	/// </summary>
	public void ApplyRestrictions(ProgressionSettings.ItemFlags restrictions) 
	{
		// ItemFlags.None is invalid
		if (restrictions == ProgressionSettings.ItemFlags.None)
		{
			Debug.LogWarning("Tried to apply empty restrictions, which is not allowed");
			return;
		}
		
		// Reset all chances to 0
		foreach (var setting in settingsCopy.AllSettings())
		{
			setting.chance = 0f;
		}

		// Set only the requested chances to 1
		if ((restrictions & ProgressionSettings.ItemFlags.Food) != 0)
			settingsCopy.FoodSettings.chance = 1f;
		if ((restrictions & ProgressionSettings.ItemFlags.Fuel) != 0)
			settingsCopy.FuelSettings.chance = 1f;
		if ((restrictions & ProgressionSettings.ItemFlags.Mechanical) != 0)
			settingsCopy.MechanicalPartsSettings.chance = 1f;
		if ((restrictions & ProgressionSettings.ItemFlags.Medicine) != 0)
			settingsCopy.MedicineSettings.chance = 1f;
	}
}
