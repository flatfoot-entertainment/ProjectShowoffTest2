using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;

[CreateAssetMenu(menuName = "Tooling/Item Spawning Settings")]
public class ItemSpawningSettings : ScriptableObject
{
	[System.Serializable]
	public class ItemTypeSettings
	{
		public ItemType type;
		public float chance;
		[FormerlySerializedAs("lowValuePrefabs")] public List<GameObject> prefabs = new List<GameObject>();

		/// <summary>
		/// Get a random item from one of the lists
		/// </summary>
		/// <returns></returns>
		public GameObject GetRandom(int value)
		{
			Item item = type switch
			{
				ItemType.Food => new Food(value),
				ItemType.MechanicalParts => new MechanicalParts(value),
				ItemType.Medicine => new Medicine(value),
				_ => new Fuel(value)
			};
			GameObject prefab = prefabs.Random();
			ItemScript script = prefab.GetComponent<ItemScript>();
			script.contained = item;
			return prefab;
		}
	}

	/// <summary>
	/// What's the value of a single item?
	/// </summary>
	public int ItemValue => itemValue;
	[SerializeField] private int itemValue = 10;
	
	/// <summary>
	/// The layer to spawn items on
	/// </summary>
	public int ItemSpawnLayer => itemSpawnLayer;
	[SerializeField, Layer] private int itemSpawnLayer;
	
	/// <summary>
	/// The settings for the items of type <see cref="ItemType.Food"/>.
	/// </summary>
	public ItemTypeSettings FoodSettings => foodSettings;
	[SerializeField] private ItemTypeSettings foodSettings = new ItemTypeSettings();

	/// <summary>
	/// The settings for the items of type <see cref="ItemType.Fuel"/>.
	/// </summary>
	public ItemTypeSettings FuelSettings => fuelSettings;
	[SerializeField] private ItemTypeSettings fuelSettings = new ItemTypeSettings();

	/// <summary>
	/// The settings for the items of type <see cref="ItemType.MechanicalParts"/>.
	/// </summary>
	public ItemTypeSettings MechanicalPartsSettings => mechanicalPartsSettings;
	[SerializeField] private ItemTypeSettings mechanicalPartsSettings = new ItemTypeSettings();
	
	/// <summary>
	/// The settings for the items of type <see cref="ItemType.Medicine"/>.
	/// </summary>
	public ItemTypeSettings MedicineSettings => medicineSettings;
	[SerializeField] private ItemTypeSettings medicineSettings = new ItemTypeSettings();

	/// <summary>
	/// An iterator over all <see cref="ItemTypeSettings"/>
	/// </summary>
	/// <returns></returns>
	public IEnumerable<ItemTypeSettings> AllSettings()
	{
		yield return foodSettings;
		yield return fuelSettings;
		yield return mechanicalPartsSettings;
		yield return medicineSettings;
	}

	/// <summary>
	/// Get the cumulative chance for an item of a specific type to spawn.
	/// </summary>
	/// <returns></returns>
	public float GetWholePercentage()
	{
		float start = 0f;
		foreach (var s in AllSettings()) start += s.chance;
		return start;
	}

	public GameObject GetRandom()
	{
		float randVal = Random.Range(0f, GetWholePercentage());
		float running = 0f;
		foreach (var set in AllSettings())
		{
			if (set.chance + running >= randVal) return set.GetRandom(itemValue);
			running += set.chance;
		}
		// Weird case, shouldn't happen
		// I like food, so in this edge case, just return a random food item
		return FoodSettings.GetRandom(itemValue);
	}

	public ItemType GetRandomType()
	{
		float randVal = Random.Range(0f, GetWholePercentage());
		float running = 0f;
		foreach (var set in AllSettings())
		{
			if (set.chance + running >= randVal) return set.type;
			running += set.chance;
		}

		// Weird case, shouldn't happen
		// I like food, so in this edge case, just return ItemType.Food
		return ItemType.Food;
	}
}
