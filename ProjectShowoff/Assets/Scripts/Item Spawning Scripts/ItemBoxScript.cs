
public class ItemBoxScript : BoxScript<ItemBox>
{
	public event System.Action AddedToBoxCallback;

	public override ItemBox contained
	{
		get => box;
		set => box = value;
	}

	private ItemBox box;

	public override void OnAddedToBox()
	{
		base.OnAddedToBox();
		AddedToBoxCallback?.Invoke();
	}
}