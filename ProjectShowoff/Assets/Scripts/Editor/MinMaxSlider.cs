﻿using UnityEditor;
using UnityEngine;

namespace FMODUnity
{
	// Copied/Adapted from https://github.com/dbrizov/NaughtyAttributes
	// The whole suite conflicts with nested List drawing in Unity
	[CustomPropertyDrawer(typeof(MinMaxSliderAttribute))]
	public class MinMaxSlider : PropertyDrawer
	{
		public override void OnGUI(Rect rect, SerializedProperty property,
			GUIContent label)
		{
			if (property.propertyType == SerializedPropertyType.Vector2 ||
				property.propertyType == SerializedPropertyType.Vector2Int)
			{
				var slider = (MinMaxSliderAttribute) attribute;
				EditorGUI.BeginProperty(rect, label, property);

				float labelWidth = EditorGUIUtility.labelWidth;
				float floatFieldWidth = EditorGUIUtility.fieldWidth;
				float sliderWidth = rect.width - labelWidth - 2.0f * floatFieldWidth;
				float sliderPadding = 5.0f;

				Rect labelRect = new Rect(
					rect.x,
					rect.y,
					labelWidth,
					rect.height);

				Rect sliderRect = new Rect(
					rect.x + labelWidth + floatFieldWidth + sliderPadding,
					rect.y,
					sliderWidth - 2.0f * sliderPadding,
					rect.height);

				Rect minFloatFieldRect = new Rect(
					rect.x + labelWidth,
					rect.y,
					floatFieldWidth,
					rect.height);

				Rect maxFloatFieldRect = new Rect(
					rect.x + labelWidth + floatFieldWidth + sliderWidth,
					rect.y,
					floatFieldWidth,
					rect.height);

				// Draw the label
				EditorGUI.LabelField(labelRect, label.text);

				// Draw the slider
				EditorGUI.BeginChangeCheck();

				if (property.propertyType == SerializedPropertyType.Vector2)
				{
					Vector2 sliderValue = property.vector2Value;
					EditorGUI.MinMaxSlider(sliderRect, ref sliderValue.x, ref sliderValue.y, slider.Min, slider.Max);

					sliderValue.x = EditorGUI.FloatField(minFloatFieldRect, sliderValue.x);
					sliderValue.x = Mathf.Clamp(sliderValue.x, slider.Min, Mathf.Min(slider.Max, sliderValue.y));

					sliderValue.y = EditorGUI.FloatField(maxFloatFieldRect, sliderValue.y);
					sliderValue.y = Mathf.Clamp(sliderValue.y, Mathf.Max(slider.Min, sliderValue.x), slider.Max);

					if (EditorGUI.EndChangeCheck())
					{
						property.vector2Value = sliderValue;
					}
				}
				else if (property.propertyType == SerializedPropertyType.Vector2Int)
				{
					Vector2Int sliderValue = property.vector2IntValue;
					float xValue = sliderValue.x;
					float yValue = sliderValue.y;
					EditorGUI.MinMaxSlider(sliderRect, ref xValue, ref yValue, slider.Min, slider.Max);

					sliderValue.x = EditorGUI.IntField(minFloatFieldRect, (int)xValue);
					sliderValue.x = (int)Mathf.Clamp(sliderValue.x, slider.Min, Mathf.Min(slider.Max, sliderValue.y));

					sliderValue.y = EditorGUI.IntField(maxFloatFieldRect, (int)yValue);
					sliderValue.y = (int)Mathf.Clamp(sliderValue.y, Mathf.Max(slider.Min, sliderValue.x), slider.Max);

					if (EditorGUI.EndChangeCheck())
					{
						property.vector2IntValue = sliderValue;
					}
				}

				EditorGUI.EndProperty();
			}
			else
			{
				EditorGUI.LabelField(rect, $"{property.propertyType} doesn't support MinMaxSliderAttribute");
			}
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return base.GetPropertyHeight(property, label);
		}
	}
}