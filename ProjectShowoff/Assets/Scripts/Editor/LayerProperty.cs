﻿using System;
using UnityEditor;
using UnityEngine;

namespace FMODUnity
{
	// Copied/Adapted from https://github.com/dbrizov/NaughtyAttributes
	// The whole suite conflicts with nested List drawing in Unity
	[CustomPropertyDrawer(typeof(LayerAttribute))]
	public class LayerProperty : PropertyDrawer
	{
		public override void OnGUI(Rect rect, SerializedProperty property,
			GUIContent label)
		{
			EditorGUI.BeginProperty(rect, label, property);
			if (property.propertyType != SerializedPropertyType.Integer)
			{
				EditorGUI.LabelField(rect, $"Layer Property not supported on {property.propertyType}");
			}
			else
			{
				int index = 0;

				string[] layers = GetLayers();
				string layerName = LayerMask.LayerToName(property.intValue);
				for (int i = 0; i < layers.Length; i++)
				{
					if (layerName.Equals(layers[i], StringComparison.Ordinal))
					{
						index = i;
						break;
					}
				}

				int newIndex = EditorGUI.Popup(rect, label.text, index, layers);
				string newLayerName = layers[newIndex];
				int newLayerNumber = LayerMask.NameToLayer(newLayerName);

				if (property.intValue != newLayerNumber)
				{
					property.intValue = newLayerNumber;
				}
			}
			EditorGUI.EndProperty();
		}

		private string[] GetLayers()
		{
			return UnityEditorInternal.InternalEditorUtility.layers;
		}
	}
}