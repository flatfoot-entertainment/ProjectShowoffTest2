﻿using UnityEditor;
using UnityEngine;

public class CheatsEditor : EditorWindow
{
	private Vector2 scrollPos;
	private int moneyToAdd;
	private float highscoreToAdd;
	
	[MenuItem("Game Tools/Cheats")]
	private static void ShowWindow()
	{
		var window = GetWindow<CheatsEditor>();
		window.titleContent = new GUIContent("Cheats");
		window.Show();
	}

	private void OnGUI()
	{
		var boldStyle = new GUIStyle {fontStyle = FontStyle.Bold, normal = {textColor = Color.white}};
		var titleStyle = new GUIStyle
		{
			fontStyle = FontStyle.Bold,
			fontSize = 15,
			normal =
			{
				textColor = Color.white
			}
		};
		EditorGUILayout.LabelField("Cheats", titleStyle);
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
		if (EditorApplication.isPlaying)
		{
			EditorGUILayout.LabelField("Money", boldStyle);
			var rect = EditorGUILayout.BeginHorizontal();
			var moneyRect = new Rect(rect.x, rect.y, (rect.width / 5f) * 3f, rect.height);
			var buttonRect = new Rect(rect.x + moneyRect.width, rect.y, (rect.width / 5f) * 2f, rect.height);
			moneyToAdd = EditorGUI.IntField(moneyRect, moneyToAdd);
			if (GUI.Button(buttonRect, "Cheat Money"))
			{
				EventScript.Handler.BroadcastEvent(new ManageMoneyEvent(moneyToAdd));
			}
			EditorGUILayout.LabelField(""); // This is somehow necessary, otherwise nothing draws
			EditorGUILayout.EndHorizontal();
			
			EditorGUILayout.LabelField("Highscore", boldStyle);
			rect = EditorGUILayout.BeginHorizontal();
			var timeRect = new Rect(rect.x, rect.y, (rect.width / 5f) * 3f, rect.height);
			buttonRect = new Rect(rect.x + moneyRect.width, rect.y, (rect.width / 5f) * 2f, rect.height);
			highscoreToAdd = EditorGUI.FloatField(timeRect, highscoreToAdd);
			if (GUI.Button(buttonRect, "Add Highscore"))
			{
				Highscores.AddEntry("CHEATED", highscoreToAdd);
			}

			EditorGUILayout.LabelField(""); // This is somehow necessary, otherwise nothing draws
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.LabelField("Win/Lose Game", boldStyle);
			if (GUILayout.Button("Win game"))
			{
				var obj = FindObjectOfType<ChangeGameState>();
				if (obj) obj.SetGameState(GameState.Ended);
				else EditorUtility.DisplayDialog("WRONG", "This scene doesn't have a ChangeGameState object!", "ok");
			}

			if (GUILayout.Button("Lose game"))
			{
				GameHandler.Instance.hasLost = true;
				var obj = FindObjectOfType<ChangeGameState>();
				if (obj) obj.SetGameState(GameState.Ended);
				else EditorUtility.DisplayDialog("WRONG", "This scene doesn't have a ChangeGameState object!", "ok");
			}
		}
		else
		{
			EditorGUILayout.HelpBox("Some options are only available when playing!", MessageType.Info);
		}

		EditorGUILayout.EndScrollView();
	}
}
