using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CraneController : MonoBehaviour
{
	[SerializeField, Tooltip("The hook of this crane.")]
	private SpringlessGrabber hook;

	[SerializeField, Tooltip("Which objects (by LayerMask) should be able to be hooked")]
	private LayerMask hookableMask;

	[SerializeField, Tooltip("The height of this train")]
	private float craneHeight;

	private Vector3 desiredPosition = Vector3.zero;

	private Plane cranePlane;

	private Camera mainCam;

	private bool canHook = false;

	private void Awake()
	{
		mainCam = Camera.main;
		EventScript.Handler.Subscribe(EventType.CameraMove ,e => OnCamMove(((CameraMoveEvent)e).NewState));
	}

	void Start()
	{
		cranePlane = new Plane(Vector3.up, new Vector3(0, craneHeight, 0));
		hook.transform.position.SetY(craneHeight);
	}

	/// <summary>
	/// Update the position where the hook should be, if it was moving
	/// </summary>
	private void UpdateDesiredPosition()
	{
		// We don't have a camera (for some reason) or the camera is disabled
		if (!mainCam || !mainCam.isActiveAndEnabled) return;
		Vector3 mousePos = Input.mousePosition;
		Ray ray = mainCam.ScreenPointToRay(mousePos);
		if (cranePlane.Raycast(ray, out float dist)) desiredPosition = ray.GetPoint(dist);
	}

	void Update()
	{
		if (!canHook) return;
		// Only update desired position 
		if (Input.GetMouseButton(0))
			UpdateDesiredPosition();

		// On mouse down, try to hook the item under the mouse
		if (Input.GetMouseButtonDown(0))
		{
			Rigidbody target = GetTarget(Input.mousePosition);
			if (target) hook.Hook(target);
		}

		// Unhook the item, if the mouse has been released
		if (Input.GetMouseButtonUp(0))
			hook.Unhook();
	}

	/// <summary>
	/// Check the object under the mouse and return it, if there is one
	/// </summary>
	private Rigidbody GetTarget(Vector3 mousePos)
	{
		// We don't have a camera (for some reason) or the camera is disabled
		if (!mainCam || !mainCam.isActiveAndEnabled) return null;
		Ray ray = mainCam.ScreenPointToRay(mousePos);
		if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, hookableMask))
		{
			return hit.rigidbody;
		}
		return null;
	}

	private void FixedUpdate()
	{
		if (hook) hook.MovePosition(desiredPosition);
	}

	private void OnCamMove(CameraMoveEvent.CameraState state)
	{
		// Check if stuff can be hooked
		// Also unhook things, if the view changes to something that isn't allowed to hook stuff
		if (state == CameraMoveEvent.CameraState.Packaging)
		{
			canHook = true;
		}
		else
		{
			canHook = false;
			hook.Unhook();
		}
	}
}
