﻿using UnityEngine;

public class ConfigAutoLoader : MonoBehaviour
{
	[SerializeField] private ConfigLoader loader;
	[SerializeField, Tooltip("The URL to the config")] private string configName;

	private void Start()
	{
		loader.LoadConfigAndContinue(configName);
	}
}
