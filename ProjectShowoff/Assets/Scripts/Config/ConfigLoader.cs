﻿using System;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConfigLoader : MonoBehaviour
{
	// https://gitlab.com/flatfoot-entertainment/hermes-config/-/raw/main/test_configs/config1.json
	[SerializeField, Scene] private string sceneToLoad;

	/// <summary>
	/// The <see cref="Config.Config"/> that has been loaded. ATTENTION: This can be null, so use ?. plentifully!
	/// </summary>
	public static Config.Config GameConfig { get; private set; }

	/// <summary>
	/// Load the config from a specific URL
	/// </summary>
	private static Config.Config LoadConfig(string url)
	{
		string json;
		try
		{
			// Try to read the file from the URL
			json = ReadFile(url);
		}
		catch (Exception)
		{
			Debug.LogWarning($"LOAD ERROR: Could not read config from URL {url}");
			return Config.Config.DefaultConfig();
		}

		try
		{
			// Try to parse the config from the read JSON and return it
			return Config.Config.FromJson(json);
		}
		catch (Exception)
		{
			Debug.LogWarning($"PARSE ERROR: Could not parse config from URL {url}");
			return Config.Config.DefaultConfig();
		}
	}

	/// <summary>
	/// Read a file from the specified URL
	/// </summary>
	private static string ReadFile(string url)
	{
		// Make a web request to the URL
		var webRequest = WebRequest.Create(url);

		// using correctly disposes of them after the method (and in an exception case)
		using var response = webRequest.GetResponse();
		using var content = response.GetResponseStream();
		using var reader = new StreamReader(content);
		return reader.ReadToEnd();
	}

	/// <summary>
	/// Load the config from the URL and load the scene that was specified in the <see cref="ConfigLoader"/>.
	/// </summary>
	public void LoadConfigAndContinue(string url)
	{
		GameConfig = LoadConfig(url);
		SceneManager.LoadScene(sceneToLoad);
	}
}
