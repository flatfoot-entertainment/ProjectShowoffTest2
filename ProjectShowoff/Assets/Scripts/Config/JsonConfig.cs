using UnityEngine;

namespace Config
{
	using System.Collections.Generic;

	using System.Globalization;
	using Newtonsoft.Json;
	using Newtonsoft.Json.Converters;

	public partial class Config
	{
		[JsonProperty("boxSettings")] public List<BoxSetting> BoxSettings { get; set; }
		[JsonProperty("upgradeSettings")] public UpgradeSettings UpgradeSettings { get; set; }
		[JsonProperty("itemSpawnSettings")] public ItemSpawnSettings ItemSpawnSettings { get; set; }
		[JsonProperty("orderSettings")] public OrderSettings OrderSettings { get; set; }
		[JsonProperty("levels")] public List<Level> Levels { get; set; }
		[JsonProperty("eventSettings")] public EventSettings EventSettings { get; set; }
		[JsonProperty("startingMoney")] public int StartingMoney { get; set; }
	}

	public partial class BoxSetting
	{
		[JsonProperty("price")] public int Price { get; set; }

		[JsonProperty("capacity")] public int Capacity { get; set; }

		[JsonProperty("profitMargin")] public int ProfitMargin { get; set; }
	}

	public partial class UpgradeSettings
	{
		[JsonProperty("conveyorUpgrades")] public List<UpgradeSetting> ConveyorUpgrades { get; set; }
		[JsonProperty("stopButtonUpgrades")] public List<UpgradeSetting> StopButtonUpgrades { get; set; }
		[JsonProperty("boxUpgrades")] public List<UpgradeSetting> BoxUpgrades { get; set; }
	}

	public partial class UpgradeSetting
	{
		[JsonProperty("price")] public int Price { get; set; }
		[JsonProperty("buyable")] public bool Buyable { get; set; }
	}

	public partial class ItemSpawnSettings
	{
		[JsonProperty("spawnTime")] public float SpawnTime { get; set; }

		[JsonProperty("fastSpawnTime")] public float FastSpawnTime { get; set; }
	}

	public partial class MinMaxInt
	{
		[JsonProperty("min")] public int Min { get; set; }
		[JsonProperty("max")] public int Max { get; set; }

		public Vector2Int ToVec() => new Vector2Int(Min, Max);
	}

	public partial class MinMaxFloat
	{
		[JsonProperty("min")] public float Min { get; set; }
		[JsonProperty("max")] public float Max { get; set; }

		public Vector2 ToVec() => new Vector2(Min, Max);
	}

	public partial class EventSettings
	{
		[JsonProperty("shake")] public MinMaxFloat Shake { get; set; }

		[JsonProperty("lightsOff")] public GravityRemoveOrLightsOff LightsOff { get; set; }

		[JsonProperty("gravityRemove")] public GravityRemoveOrLightsOff GravityRemove { get; set; }

		[JsonProperty("conveyorOverload")] public ConveyorOverload ConveyorOverload { get; set; }

		[JsonProperty("timeBetweenEvents")] public float TimeBetweenEvents { get; set; }
	}

	public partial class ConveyorOverload
	{
		[JsonProperty("duration")] public float Duration { get; set; }

		[JsonProperty("speedup")] public float Speedup { get; set; }
	}

	public partial class GravityRemoveOrLightsOff
	{
		[JsonProperty("duration")] public float Duration { get; set; }
	}

	public partial class Level
	{
		[JsonProperty("ordersUntilNextLevel")] public uint OrdersUntilNextLevel { get; set; }
		[JsonProperty("allowedItems")] public ProgressionSettings.ItemFlags AllowedItems { get; set; }
		[JsonProperty("availablePlanets")] public int AvailablePlanets { get; set; }
		[JsonProperty("orderSize")] public MinMaxInt OrderSize { get; set; }
		[JsonProperty("parallelOrders")] public int ParallelOrders { get; set; }
		[JsonProperty("orderValue")] public int OrderValue { get; set; }

		public ProgressionSettings.Level ToLevel()
		{
			return new ProgressionSettings.Level(OrdersUntilNextLevel, AllowedItems, AvailablePlanets, OrderSize.ToVec(),
				ParallelOrders, OrderValue);
		}
	}

	public partial class OrderSettings
	{
		[JsonProperty("timePerOrder")] public MinMaxInt TimePerOrder { get; set; }
		[JsonProperty("timeBetweenOrders")] public MinMaxFloat TimeBetweenOrders { get; set; }
	}

	public partial class Config
	{
		public static Config FromJson(string json) =>
			JsonConvert.DeserializeObject<Config>(json, Converter.Settings);
	}

	public static class Serialize
	{
		public static string ToJson(this Config self) => JsonConvert.SerializeObject(self, Converter.Settings);
	}

	internal static class Converter
	{
		public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
		{
			MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
			DateParseHandling = DateParseHandling.None,
			Converters =
			{
				new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
			},
		};
	}
}

namespace Config
{
	public partial class Config
	{
		public static Config DefaultConfig()
		{
			return new Config();
		}
	}
}
