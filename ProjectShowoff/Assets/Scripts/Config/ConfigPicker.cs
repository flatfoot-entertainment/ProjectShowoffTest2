﻿using TMPro;
using UnityEngine;

public class ConfigPicker : MonoBehaviour
{
	[SerializeField] private string urlFormat;
	[SerializeField] private TMP_InputField input;
	[SerializeField] private ConfigLoader loader;

	public void Yeet()
	{
		loader.LoadConfigAndContinue(string.Format(urlFormat, input.text));
	}
}
