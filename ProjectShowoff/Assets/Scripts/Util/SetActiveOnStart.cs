﻿using System.Collections.Generic;
using UnityEngine;

public class SetActiveOnStart : MonoBehaviour
{
	[SerializeField] private List<GameObject> affected;
	[SerializeField] private bool state;

	private void Start()
	{
		foreach (var o in affected)
		{
			if (o) o.SetActive(state);
		}
	}
}