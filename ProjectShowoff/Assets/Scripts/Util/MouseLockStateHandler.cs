﻿using UnityEngine;

public class MouseLockStateHandler : MonoBehaviour
{
	[SerializeField, Tooltip("Changes will only be registered, the next time the game is played/gains focus")]
	private CursorLockMode lockMode;

	private void Start()
	{
		Cursor.lockState = lockMode;
		Cursor.visible = true;
	}

	private void OnApplicationFocus(bool hasFocus)
	{
		if (hasFocus)
		{
			Cursor.lockState = lockMode;
			Cursor.visible = true;
		}
	}
}