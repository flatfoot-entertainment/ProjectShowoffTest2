﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

[CreateAssetMenu(fileName = "ProgressionSettings", menuName = "Progression/Settings", order = 0)]
public class ProgressionSettings : ScriptableObject
{
	[Flags, Serializable]
	[JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
	public enum ItemFlags
	{
		None = 0b0,
		Food = 0b1,
		Fuel = 0b10,
		Medicine = 0b100,
		Mechanical = 0b1000,
		Everything = Food | Fuel | Medicine | Mechanical
	}

	[Serializable]
	public class Level
	{
		[SerializeField, Min(1)] private uint ordersUntilNextLevel;
		/// <summary>
		/// How many orders are there until the next level
		/// </summary>
		public uint OrdersUntilNextLevel => ordersUntilNextLevel;

		[SerializeField] private ItemFlags allowedItems;
		/// <summary>
		/// Which items are allowed to spawn in this level
		/// </summary>
		public ItemFlags AllowedItems => allowedItems;

		[SerializeField, Range(1, 4)] private int availablePlanets;
		/// <summary>
		/// How many planets are allowed to have orders in this level
		/// </summary>
		public int AvailablePlanets => availablePlanets;

		[SerializeField, MinMaxSlider(1, 50)] private Vector2Int orderSize;
		/// <summary>
		/// How big orders can be in this level
		/// </summary>
		public Vector2Int OrderSize => orderSize;

		[SerializeField, Range(1, 4)] private int parallelOrders;
		/// <summary>
		/// How many orders are allowed at once in this level
		/// </summary>
		public int ParallelOrders => parallelOrders;

		[SerializeField, Range(20, 300)] private int orderValue;
		/// <summary>
		/// Whats the value of a completed order
		/// </summary>
		public int OrderValue => orderValue;

		public Level()
		{
			ordersUntilNextLevel = 1;
			allowedItems = ItemFlags.Everything;
			availablePlanets = 1;
			orderSize = new Vector2Int(3, 6);
			parallelOrders = 1;
			orderValue = 20;
		}

		public Level(uint ordersUntilNextLevel, ItemFlags allowedItems, int availablePlanets, Vector2Int orderSize,
			int parallelOrders, int orderValue)
		{
			this.ordersUntilNextLevel = ordersUntilNextLevel;
			this.allowedItems = allowedItems;
			this.availablePlanets = availablePlanets;
			this.orderSize = orderSize;
			this.parallelOrders = parallelOrders;
			this.orderValue = orderValue;
		}
	}

	[SerializeField] private List<Level> levels;
	public List<Level> Levels => levels;
}