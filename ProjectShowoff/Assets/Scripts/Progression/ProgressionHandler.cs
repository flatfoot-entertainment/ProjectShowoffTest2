﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Config;
using UnityEngine;

public class ProgressionHandler : MonoBehaviour
{
	[SerializeField] private ProgressionSettings settings;
	[SerializeField] private PercentageItemFactory itemFactory;
	[SerializeField] private OrdersManager ordersManager;
	private IEnumerator<ProgressionSettings.Level> levelsIterator;
	private List<ProgressionSettings.Level> levels;
	private ProgressionSettings.Level currentLevel;

	private int completedShipments = 0;

	private void Start()
	{
		Initialize();

		EventScript.Handler.Subscribe(EventType.OrderComplete, _ => OnOrderCompleted());
	}

	private void Initialize()
	{
		completedShipments = 0;

		// Convert the levels from the config into ProgressionSettings' levels
		var configLevelsList = ConfigLoader.GameConfig?.Levels ?? new List<Level>();
		var configLevels = configLevelsList.ConvertAll(l => l.ToLevel());

		var count = Mathf.Max(settings.Levels.Count, configLevels.Count);

		levels = new List<ProgressionSettings.Level>();

		for (var i = 0; i < count; i++)
		{
			levels.Add(configLevels.Count > i ? configLevels[i] : settings.Levels[i]);
		}
		levelsIterator = levels.GetEnumerator();
		NextLevel();
	}

	private void OnOrderCompleted()
	{
		// An order has been completed, register and handle all the necessary stuff
		completedShipments++;
		EventScript.Handler.BroadcastEvent(new ManageMoneyEvent(currentLevel.OrderValue));
		Debug.Log($"Completed shipments: {completedShipments}");
		Debug.Log($"next level: {currentLevel.OrdersUntilNextLevel}");
		if (completedShipments < currentLevel.OrdersUntilNextLevel) return;
		Debug.Log("Next level");
		completedShipments = 0;
		NextLevel();
	}

	private void NextLevel()
	{
		if (levelsIterator.MoveNext()) currentLevel = levelsIterator.Current;
		else
		{
			// if MoveNext returns false, there is no next level, so the game has ended
			GameHandler.Instance.GetComponent<ChangeGameState>().SetGameState(GameState.Ended);
			return;
		}
		if (currentLevel == null)
		{
			Debug.LogError("Could not retrieve a next level, which means the level list in the settings is empty!", settings);
			throw new ArgumentNullException();
		}
		// Apply all the things from the new level
		itemFactory.ApplyRestrictions(currentLevel.AllowedItems);
		ordersManager.OnNewLevel(
			currentLevel.ParallelOrders,
			currentLevel.AvailablePlanets,
			currentLevel.OrderSize
		);
	}
}
