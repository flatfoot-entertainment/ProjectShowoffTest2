using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using Cinemachine;
using Random = UnityEngine.Random;
public class GameHandler : MonoBehaviour
{
	[SerializeField] private RandomEventScript randomEventScript;

	/// <summary>
	/// How many upgrades did the player buy so far? (Used for switching the ships)
	/// </summary>
	public int UpgradeIndex
	{
		get => upgradeIndex;
		set => upgradeIndex = value;
	}
	public SpawnerController SpawnerController => spawnerController;
	[SerializeField] private TextMeshProUGUI moneyText;
	[SerializeField] private int money;

	[SerializeField] private GameObject[] conveyorStopButtons;

	[SerializeField] private GameObject[] leftConveyorBelts, rightConveyorBelts;
	[SerializeField] private GameObject rightConveyorsContainer;

	// TODO global/public variables are kinda iffy -> Refactor
	public bool hasLost = false;

	[SerializeField] private SpawnerController spawnerController;

	[SerializeField] private string moneyTextFormat = "¤{0}";


	[SerializeField] private int upgradeIndex;

	public static GameHandler Instance;

	private float initialSpawnInterval;
	/// <summary>
	/// The amount of money the player has
	/// </summary>
	public int Money
	{
		get => money;
		set => money = value;
	}

	private void Awake()
	{
		if (Instance != null && Instance != this)
		{
			Destroy(gameObject);
		}
		else
		{
			Instance = this;
		}
	}

	protected virtual void Start()
	{
		money = ConfigLoader.GameConfig?.StartingMoney ?? money;
		EventScript.Handler.Subscribe(EventType.ManageMoney, ManageMoney);
		EventScript.Handler.Subscribe(EventType.ManageUpgrade, OnUpgradeBought);
		EventScript.Handler.Subscribe(EventType.ConveyorUpgrade, UpgradeConveyorBelt);
		EventScript.Handler.Subscribe(EventType.ConveyorStopButtonUpgrade, _ => EnableConveyorButton());
		moneyText.text = string.Format(moneyTextFormat, money.ToString());
		spawnerController = GetComponent<SpawnerController>();
		randomEventScript = GetComponent<RandomEventScript>();
		initialSpawnInterval = spawnerController.SpawnInterval;
		Time.timeScale = 0f;

		rightConveyorsContainer.SetActive(false);
	}

	private void OnDestroy()
	{
		OnDestroyCallback();
	}

	private void ManageMoney(Event e)
	{
		if (!(e is ManageMoneyEvent moneyEvent)) return;
		money += moneyEvent.Amount;
		moneyText.text = string.Format(moneyTextFormat, money.ToString());
	}

	private void ManageUpgrade(Event e)
	{
		if (!(e is ManageUpgradeEvent upgradeEvent)) return;
		money -= upgradeEvent.Upgrade.Cost;
		moneyText.text = string.Format(moneyTextFormat, money.ToString());
	}

	protected virtual void OnDestroyCallback()
	{
		EventScript.Handler.UnsubscribeAll();
	}

	protected virtual void OnUpgradeBought(Event e)
	{
		ManageUpgrade(e);
	}

	/// <summary>
	/// Enable the conveyor stop buttons
	/// </summary>
	private void EnableConveyorButton()
	{
		foreach (GameObject conveyorButton in conveyorStopButtons)
		{
			conveyorButton.SetActive(true);
		}
	}

	/// <summary>
	/// Upgrade the conveyors to the specified level
	/// </summary>
	private void UpgradeConveyorBelt(Event e)
	{
		if (!(e is ConveyorUpgradeEvent upgrade)) return;
		int level = upgrade.Level;
		SpawnerController spawnerController = GetComponent<SpawnerController>();
		if (spawnerController)
		{
			switch (level)
			{
				case 1:
					rightConveyorsContainer.SetActive(true);
					rightConveyorBelts[0].SetActive(true);
					spawnerController.AddSpawner(rightConveyorBelts[0].GetComponentInChildren<ItemSpawner>());
					spawnerController.AddConveyor(rightConveyorBelts[0].GetComponentInChildren<ConveyorSetupScript>());
					break;
				case 2:
					spawnerController.RemoveConveyorAt(0);
					spawnerController.RemoveSpawnerAt(0);
					leftConveyorBelts[0].SetActive(false);

					leftConveyorBelts[1].SetActive(true);
					spawnerController.AddSpawners(leftConveyorBelts[1].GetComponentsInChildren<ItemSpawner>());
					spawnerController.AddConveyor(leftConveyorBelts[1].GetComponentInChildren<ConveyorSetupScript>());
					break;
				case 3:
					spawnerController.RemoveConveyorAt(0);
					spawnerController.RemoveSpawnerAt(0);
					rightConveyorBelts[0].SetActive(false);

					rightConveyorBelts[1].SetActive(true);
					spawnerController.AddSpawners(rightConveyorBelts[1].GetComponentsInChildren<ItemSpawner>());
					spawnerController.AddConveyor(rightConveyorBelts[1].GetComponentInChildren<ConveyorSetupScript>());
					break;
			}
		}
		else Debug.LogError("SpawnerController not found when upgrading conveyor belts");
	}

	/// <summary>
	/// Call this after the tutorial is finished. This will property start the game
	/// (set <see cref="Time.timeScale"/>, etc.)
	/// </summary>
	public void StartGame()
	{
		Time.timeScale = 1f;
		randomEventScript.StartRandomEvents();
		StartCoroutine(spawnerController.Coroutine());
		GetComponent<ChangeGameState>().GameState = GameState.Ingame;
		EventScript.Handler.BroadcastEvent(new StartGameEvent());
	}
}
