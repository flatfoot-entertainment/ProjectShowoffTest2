using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//The class that handles creation of orders for planets
public class OrdersManager : MonoBehaviour
{
	[SerializeField] private Planet[] planets;
	private int maxPlanetIndex = 1;
	[SerializeField, MinMaxSlider(10, 40)] private Vector2Int moneyLostPerOrder;
	[SerializeField, MinMaxSlider(2, 180)] private Vector2Int timePerOrder;
	private Vector2Int TimePerOrder => ConfigLoader.GameConfig?.OrderSettings?.TimePerOrder.ToVec() ?? timePerOrder;
	[SerializeField, MinMaxSlider(0f, 45f)]
	private Vector2 timeBetweenOrders;

	private Vector2 TimeBetweenOrders => ConfigLoader.GameConfig?.OrderSettings?.TimeBetweenOrders.ToVec() ?? timeBetweenOrders;

	private int maxParallelOrders;
	private List<IEnumerator> orders = new List<IEnumerator>();

	private void Start()
	{
		EventScript.Handler.Subscribe(EventType.StartGame, _ => StartCoroutine(OrderLoop()));
	}

	/// <summary>
	/// This coroutine starts new orders, once spots are free
	/// </summary>
	private IEnumerator OrderLoop()
	{
		while (gameObject)
		{
			StartCoroutine(OrderWrapper());
			yield return new WaitWhile(() => orders.Count >= Mathf.Min(maxParallelOrders, maxPlanetIndex));
		}
	}

	/// <summary>
	/// Start an order, add it to the list and remove it again, once it's done
	/// </summary>
	/// <returns></returns>
	private IEnumerator OrderWrapper()
	{
		var order = NewOrder();
		orders.Add(order);
		yield return order;
		orders.Remove(order);
	}

	/// <summary>
	/// The actual thing doing the order stuff
	/// </summary>
	/// <returns></returns>
	private IEnumerator NewOrder()
	{
		// First wait for a random amount of time
		yield return new WaitForSeconds(TimeBetweenOrders.Random());
		// Check out which planets are free
		var free = planets.Where(p => !p.HasOrder).ToList();
		if (free.Count > 0)
		{
			// We don't have to check if planets are free, as LINQ has that guaranteed
			Planet planet = free.Random(maxPlanetIndex);

			planet.NewOrder(TimePerOrder.Random(), moneyLostPerOrder.Random());
			yield return new WaitWhile(() => planet.HasOrder);
		}
	}

	/// <summary>
	/// The player has reached a new progression level, so call this function to set all the parameters affected.
	/// </summary>
	public void OnNewLevel(int newMaxParallelOrders, int newLimit, Vector2Int orderSize)
	{
		maxParallelOrders = newMaxParallelOrders;
		maxPlanetIndex = Mathf.Max(newLimit, 1);
		foreach (var planet in planets) planet.UpdateOrderSize(orderSize);
	}
}
