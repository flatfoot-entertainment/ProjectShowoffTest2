using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum GameState
{
	MainMenu,
	Ingame,
	Ended,
	Paused,
}
public class ChangeGameState : MonoBehaviour
{
	/// <summary>
	/// The current game state of the game
	/// </summary>
	public GameState GameState
	{
		get => gameState;
		set => gameState = value;
	}

	public bool CannotContinue => boxSelectionThing.PlayerHasNoBackupBox;

	/// <summary>
	/// The current time spent in an active game
	/// </summary>
	public float TimeSpent => timeSpent;
	/// <summary>
	/// The minimal amount of money, with which the player can survive without having a box
	/// </summary>
	public float MinimalAmountOfMoney => minimalAmountOfMoney;

	[SerializeField] private GameObject gameLostPanel, gameWonPanel;
	[SerializeField] private BoxSelectionScript boxSelectionThing;
	[SerializeField] private ShipComponents ship; // Because we need a web of nonsensical references through the project
	[SerializeField] private float timeSpent;
	[SerializeField] private GameState gameState;

	private float minimalAmountOfMoney;

	private bool endFlag = false;

	private void Awake()
	{
		// It has to be set somewhere, so why not here?
		Highscores.HighscoreLimit = 10;
	}

	private void Start()
	{
		minimalAmountOfMoney = boxSelectionThing.MinimumMoneyNeeded;
		EventScript.Handler.Subscribe(EventType.ManageMoneyLeft, _ => OnMoneyLeft());
	}

	private void Update()
	{
		if (endFlag) return;
		//probably not the best place to check stuff, but eh, gotta refactor this anyway
		if (gameState == GameState.Ingame)
		{
			//start counting time once game has started
			timeSpent += Time.deltaTime;
		}
		else if (gameState == GameState.Ended)
		{
			endFlag = true;
			if (GameHandler.Instance.hasLost)
			{
				gameLostPanel.SetActive(true);
			}
			else
			{
				gameWonPanel.SetActive(true);
			}
			Time.timeScale = 0f;
		}
	}
	/// <summary>
	/// Set the game state to the specified one. (The int will be cast to <see cref="GameState"/>)
	/// <br/>
	/// This is primarily used in the editor, as Unity is stupid and doesn't allow enums as
	/// function parameters in events.
	/// </summary>
	public void SetGameState(int state)
	{
		gameState = (GameState)state;
	}

	/// <summary>
	/// Set the game state to the specified one
	/// </summary>
	public void SetGameState(GameState state)
	{
		gameState = state;
	}

	/// <summary>
	/// Pause the game. This will also set <see cref="Time.timeScale"/> to 0.
	/// </summary>
	public void PauseGame()
	{
		gameState = GameState.Paused;
		Time.timeScale = 0f;
	}

	/// <summary>
	/// Unpause the game. This will also set <see cref="Time.timeScale"/> to 1.
	/// </summary>
	public void UnpauseGame()
	{
		gameState = GameState.Ingame;
		Time.timeScale = 1f;
	}

	private void OnMoneyLeft()
	{
		if (!(GameHandler.Instance.Money < minimalAmountOfMoney) || !boxSelectionThing.PlayerHasNoBackupBox ||
			ship.CanShip) return;
		SetGameState(GameState.Ended);
		GameHandler.Instance.hasLost = true;
	}
}
