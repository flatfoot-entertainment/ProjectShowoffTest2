using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class BlinkingButtonScript : MonoBehaviour
{
	[SerializeField] private EventType blinkType;
	[SerializeField] private bool isBlinking;
	[SerializeField] private Image image;
	[SerializeField] private float blinkSpeed = 2f;
	[SerializeField] private Color blinkColor = Color.red;
	private void Start()
	{
		image = GetComponent<Image>();
		EventScript.Handler.Subscribe(EventType.ButtonBlinkEvent, OnBlinking);
	}

	// Update is called once per frame
	private void Update()
	{
		Color c = image.color;
		if (isBlinking)
		{
			c = Color.Lerp(Color.white, blinkColor, Mathf.Abs(Mathf.Sin(Time.time * blinkSpeed)));
		}
		else
		{
			c = Color.white;
		}
		image.color = c;
	}

	public void StopBlinking()
	{
		EventScript.Handler.BroadcastEvent(new ManageButtonBlinkingEvent(blinkType, false));
	}

	private void OnBlinking(Event e)
	{

		if (!(e is ManageButtonBlinkingEvent blinkingEvent)) return;
		if (blinkingEvent.Affected == blinkType) isBlinking = blinkingEvent.IsBlinking;
	}
}
