using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;

public enum RandomEvent
{
	Shake,
	ConveyorOverload,
	GravityRemove
}
public class RandomEventScript : MonoBehaviour
{
	[SerializeField] private float minRandomShake;

	[SerializeField] private float maxRandomShake;

	private float MinRandomShake => ConfigLoader.GameConfig?.EventSettings?.Shake?.Min ?? minRandomShake;
	private float MaxRandomShake => ConfigLoader.GameConfig?.EventSettings?.Shake?.Max ?? maxRandomShake;

	[SerializeField] private float conveyorOverloadMultiplier;
	[SerializeField] private CinemachineVirtualCamera virtualCamera;
	[SerializeField] private Transform itemsSpawned;
	[SerializeField] private float gravityRemoveDuration;
	[SerializeField] private float conveyorOverloadDuration;
	[SerializeField] private float conveyorSpeedUpValue;
	private float GravityRemoveDuration => ConfigLoader.GameConfig?.EventSettings?.GravityRemove?.Duration ?? gravityRemoveDuration;
	private float ConveyorOverloadDuration => ConfigLoader.GameConfig?.EventSettings?.ConveyorOverload?.Duration ?? conveyorOverloadDuration;
	private float ConveyorSpeedUpValue => ConfigLoader.GameConfig?.EventSettings?.ConveyorOverload?.Speedup ?? conveyorSpeedUpValue;
	[SerializeField] private GameObject EmergencyTrigger;
	[SerializeField] private GameObject packagingTrigger;
	[SerializeField] private GameObject planetTrigger;
	[SerializeField] private float randomEventOccurance;
	private float RandomEventOccurance =>
		ConfigLoader.GameConfig?.EventSettings?.TimeBetweenEvents ?? randomEventOccurance;
	[SerializeField] private GameObject warningEventImage;
	private RandomEvent randomEvent;

	private CameraMoveEvent.CameraState current;

	private void Start()
	{
		itemsSpawned = GameObject.Find("Items Spawned").transform;
		EventScript.Handler.Subscribe(EventType.CameraMove, e => current = ((CameraMoveEvent)e).NewState);
	}
	private IEnumerator DoRandomEvent()
	{
		randomEvent = Extensions.RandomEnumValue<RandomEvent>();
		// randomEvent = RandomEvent.ConveyorOverload;
		yield return new WaitForSeconds(RandomEventOccurance - 3f);
		warningEventImage.SetActive(true);
		EmergencyTrigger.SetActive(true);
		planetTrigger.SetActive(false);
		packagingTrigger.SetActive(false);
		EventScript.Handler.BroadcastEvent(new ManageBlinkEvent(randomEvent));
		yield return new WaitForSeconds(0.5f);
		warningEventImage.GetComponent<BlinkScript>().IsMovingToEndPos = true;
		yield return new WaitForSeconds(2.5f);
		warningEventImage.SetActive(false);

		switch (randomEvent)
		{
			case RandomEvent.Shake:
				StartCoroutine(DoShake());
				break;
			case RandomEvent.GravityRemove:
				DoGravityRemove(GravityRemoveDuration);
				break;
			case RandomEvent.ConveyorOverload:
				DoConveyorOverload(ConveyorOverloadDuration);
				break;
		}
	}

	private IEnumerator DoShake()
	{
		foreach (Transform item in itemsSpawned)
		{
			var itemScript = item.GetComponent<ItemScript>();
			if (itemScript && itemScript.CanShake)
			{
				Rigidbody itemRb = item.GetComponent<Rigidbody>();
				if (itemRb)
				{
					ApplyRandomForce(itemRb);
				}
				else
				{
					Debug.LogError("[censored] the items dont have rigidbodies");
				}
			}
		}
		virtualCamera.transform.DOShakePosition(1f, 1, 7, 90, false, true);
		EmergencyTrigger.SetActive(false);
		switch (current)
		{
			case CameraMoveEvent.CameraState.Packaging:
				planetTrigger.SetActive(false);
				packagingTrigger.SetActive(true);
				break;
			case CameraMoveEvent.CameraState.Shipping:
				planetTrigger.SetActive(true);
				packagingTrigger.SetActive(false);
				break;
		}
		//playoneshot
		yield return DoRandomEvent();
	}

	private void DoGravityRemove(float duration)
	{
		StartCoroutine(GravityRemove(duration));
	}

	private void DoConveyorOverload(float duration)
	{
		StartCoroutine(ConveyorOverload(duration));
	}

	private IEnumerator GravityRemove(float duration)
	{
		Physics.gravity = -Vector3.up;
		yield return new WaitForSeconds(duration);
		Physics.gravity = new Vector3(0f, -9.81f, 0f);
		EmergencyTrigger.SetActive(false);
		yield return DoRandomEvent();
	}

	private IEnumerator ConveyorOverload(float duration)
	{
		GameHandler.Instance.SpawnerController.ChangeConveyorSpeed(ConveyorSpeedUpValue);
		EventScript.Handler.BroadcastEvent(new ManageConveyorAnimatorEvent(ConveyorSpeedUpValue));
		GameHandler.Instance.SpawnerController.SpawnInterval =
			ConfigLoader.GameConfig?.ItemSpawnSettings?.FastSpawnTime ?? GameHandler.Instance.SpawnerController.InitialSpawnInterval / conveyorOverloadMultiplier;

		yield return new WaitForSeconds(duration);
		EmergencyTrigger.SetActive(false);
		GameHandler.Instance.SpawnerController.ChangeConveyorSpeed(1f);
		GameHandler.Instance.SpawnerController.SpawnInterval = ConfigLoader.GameConfig?.ItemSpawnSettings?.SpawnTime ??
										  GameHandler.Instance.SpawnerController.InitialSpawnInterval;
		EventScript.Handler.BroadcastEvent(new ManageConveyorAnimatorEvent(1f));
		yield return DoRandomEvent();
	}

	private void ApplyRandomForce(Rigidbody rb)
	{
		Vector3 rand = Random.onUnitSphere * Random.Range(MinRandomShake, MaxRandomShake);
		Vector3 randAbs = new Vector3(Mathf.Abs(rand.x), Mathf.Abs(rand.y), Mathf.Abs(rand.z));
		rb.AddForce(randAbs, ForceMode.Impulse);
	}


	public void StartRandomEvents()
	{
		StartCoroutine(DoRandomEvent());
	}

}
