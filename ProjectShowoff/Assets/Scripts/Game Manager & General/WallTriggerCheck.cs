using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;


public class WallTriggerCheck : MonoBehaviour
{
	[SerializeField] private LayerMask respawnLayerMask;
	[SerializeField] private Transform boxSpawnPos;

	private void OnTriggerEnter(Collider other)
	{
		if (respawnLayerMask.Contains(other.gameObject.layer))
		{
			other.gameObject.transform.position = boxSpawnPos.position;
			other.gameObject.transform.rotation = Quaternion.identity;
			other.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
		}
	}
}
