using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

//This is the blinking script for the warning image, not to be confused with BlinkingButtonScript (probably needs refactoring)
public class BlinkScript : MonoBehaviour
{
	public bool IsMovingToEndPos;
	private Image image;

	[SerializeField] private Transform startWarningPos, finalWarningPos;
	[SerializeField] private float blinkSpeed = 2f;
	[SerializeField] private Sprite shakeSprite, overloadSprite, gravitySprite;
	[SerializeField] private Color blinkColor;
	private void Awake()
	{
		image = GetComponent<Image>();
		EventScript.Handler.Subscribe(EventType.ManageBlinkEvent, OnRandomEventReceived);
		transform.position = startWarningPos.position;
	}

	private void Update()
	{
		Color c = image.color;
		c = Color.Lerp(Color.white, blinkColor, Mathf.Abs(Mathf.Sin(Time.time * blinkSpeed)));
		image.color = c;
		if (IsMovingToEndPos)
		{
			transform.DOMove(finalWarningPos.position, 1f);
			IsMovingToEndPos = false;
		}
	}

	private void OnDisable()
	{
		IsMovingToEndPos = false;
		transform.position = startWarningPos.position;
	}

	private void OnRandomEventReceived(Event e)
	{
		ManageBlinkEvent blinkEvent = e as ManageBlinkEvent;
		//Sprite blinkSprite = null;
		switch (blinkEvent.RandomEvent)
		{
			case RandomEvent.Shake:
				image.sprite = shakeSprite;
				break;
			case RandomEvent.ConveyorOverload:
				image.sprite = overloadSprite;
				break;
			case RandomEvent.GravityRemove:
				image.sprite = gravitySprite;
				break;
			default:
				break;
		}
	}

}
