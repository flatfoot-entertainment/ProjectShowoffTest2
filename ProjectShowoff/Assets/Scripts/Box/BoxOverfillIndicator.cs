﻿using UnityEngine;
using DG.Tweening;

public class BoxOverfillIndicator : MonoBehaviour
{
	[SerializeField] private Renderer indicator;
	
	/// <summary>
	/// Should the indicator be shown? (It is shown if the contents are NOT valid)
	/// </summary>
	/// <param name="valid"></param>
	public void ChangeState(bool valid)
	{
		indicator.material.color = new Color(1, 0, 0, 0.4f);
		indicator.gameObject.SetActive(!valid);
	}
}
