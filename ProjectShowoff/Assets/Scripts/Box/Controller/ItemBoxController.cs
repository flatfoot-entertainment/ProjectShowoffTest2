using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBoxController : BoxController<ItemBox, Item>
{
	[SerializeField, Tooltip("The prefab to replace this box with after closing")]
	private ShippableItemBox shippableBoxPrefab;
	[SerializeField] private AnimationClip closingAnimation;

	private ItemBox box;
	protected override void OnAwake()
	{
		base.OnAwake();
		box = new ItemBox();
	}

	public override ItemBox Box
	{
		get => box;
		protected set => box = value;

	}

	/// <inheritdoc/>
	protected override void PurgeContents()
	{
		// Items are spawned using LeanPool, so they have to be de-spawned with it as well
		foreach (GameObject gO in contained) Lean.Pool.LeanPool.Despawn(gO);
	}

	/// <summary>
	/// Call this when the box is ready to be replaced by the prefab. (animations are done, etc.)
	/// </summary>
	private void Ship()
	{
		// Create a new shippable variant
		var shippable = Instantiate(shippableBoxPrefab, transform.position, transform.rotation, transform.parent);

		// Move box to shippable instance
		shippable.Init(Box);
		Box = null;

		// Enable shippable and destroy self
		shippable.gameObject.SetActive(true);
		Destroy(gameObject);
	}

	/// <summary>
	/// Close the box. This will play all the necessary animations and spawn the correct prefab.
	/// </summary>
	public void Close()
	{
		// Set the layer to default, so the items can't be picked up anymore
		foreach (var o in contained) o.layer = 0;
		StartCoroutine(CloseBox());
		lid.Close();
	}

	private IEnumerator CloseBox()
	{
		// Start the animation, wait for it to finish and call Ship
		Animator boxAnimator = GetComponentInChildren<Animator>();
		if (boxAnimator == null) Debug.LogError("BoxAnimator not found.");
		else
		{
			boxAnimator.SetBool("isClosing", true);
			yield return new WaitForSeconds(closingAnimation.length);
			Ship();
		}
	}
}
