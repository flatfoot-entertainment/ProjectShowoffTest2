using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class BoxController<BoxT, Contained> : MonoBehaviour where BoxT : IBox<Contained>
{
	/// <summary>
	/// Called when the contents of the box change
	/// </summary>
	public event System.Action OnContentsUpdated;

	/// <summary>
	/// The lid object of the Box. In combination with the <see cref="body"/>, this can reliably tell, which items
	/// are completely in the box and which aren't
	/// </summary>
	protected BoxLid<Contained> lid;
	/// <summary>
	/// The body object of the box. In combination with the <see cref="lid"/>, this can reliably tell, which items
	/// are completely in the box and which aren't
	/// </summary>
	private BoxBody body;
	/// <summary>
	/// Enable this, to show to the player, that the box has items sticking out of it
	/// </summary>
	private BoxOverfillIndicator indicator;
	/// <summary>
	/// An object, inheriting from <see cref="IBox{Contained}"/>, which contains all the actual things of
	/// type <see cref="Contained"/>
	/// </summary>
	public abstract BoxT Box { get; protected set; }
	/// <summary>
	/// The list of <see cref="GameObject"/> in the Box.
	/// </summary>
	protected List<GameObject> contained = new List<GameObject>();

	/// <summary>
	/// Can the box be shipped currently? This basically means, that no item is sticking out and the box is not empty.
	///
	/// This is only definitely valid AFTER <see cref="Start"/>
	/// </summary>
	public bool Shippable { get; private set; }

	private void Awake()
	{
		// Get all the necessary components, which are all on this GameObjects or it's children
		lid = GetComponentInChildren<BoxLid<Contained>>();
		body = GetComponentInChildren<BoxBody>();
		indicator = GetComponentInChildren<BoxOverfillIndicator>();
		// Notify potential inheritors about the Awake event
		OnAwake();
	}

	/// <summary>
	/// Inherit from this function to get notified about the <see cref="Awake"/> Unity Event.
	/// </summary>
	protected virtual void OnAwake() { }

	private void Start()
	{
		// Subscribe to the necessary callbacks
		lid.OnExitCallback += LidExit;
		lid.OnEnterCallback += LidEnter;
		body.OnContentsUpdated += OnBoxContentsUpdated;
		// Make sure that Shippable is set correctly in the beginning
		UpdateShippable();
	}

	private void UpdateShippable()
	{
		// Check if any item in the lid is also in the body, which means there are items not completely in the box
		// -> This means the box is not closable
		foreach (BoxScript<Contained> s in lid.inLid)
		{
			if (body.Has(s.gameObject))
			{
				Shippable = false;
				indicator.ChangeState(Shippable);
				return;
			}
		}
		// An empty box is not shippable
		Shippable = contained.Count > 0;
		// The indicator should only be red when theres something intersecting with it
		indicator.ChangeState(true);
	}

	private void OnDestroy()
	{
		// Unsubscribe from the necessary callbacks
		lid.OnExitCallback -= LidExit;
		lid.OnEnterCallback -= LidEnter;
		body.OnContentsUpdated -= OnBoxContentsUpdated;
		// Purge contents
		PurgeContents();
	}

	/// <summary>
	/// This method is called on destroy and should destroy all of it's contained GameObjects.
	/// <br/>
	/// This is not done in <see cref="OnDestroy"/> itself, as the inheritor might want to destroy the contained items
	/// in different ways, e.g. using <see cref="Lean.Pool.LeanPool.Despawn(GameObject, float)"/>.
	/// </summary>
	protected abstract void PurgeContents();

	/// <summary>
	/// Called when an item has left the <see cref="lid"/>
	/// </summary>
	/// <param name="subject">The item that has left the <see cref="lid"/></param>
	private void LidExit(BoxScript<Contained> subject)
	{
		if (!subject) return;
		// If the thing exiting the lid is in the body, it is fully in the box, so the contents are updated
		if (body.Has(subject.gameObject))
		{
			Box.AddToBox(subject.contained);
			contained.Add(subject.gameObject);
			subject.OnAddedToBox();
			OnContentsUpdated?.Invoke();
		}
		UpdateShippable();
	}

	/// <summary>
	/// Called when an item has entered the <see cref="lid"/>
	/// </summary>
	/// <param name="subject">The item that has entered the <see cref="lid"/></param>
	private void LidEnter(BoxScript<Contained> subject)
	{
		if (!subject) return;
		// If the thing entering the lid is in the body, it is not fully in the box anymore, so the contents are updated
		if (body.Has(subject.gameObject))
		{
			// subject.transform.parent = null;
			Box.RemoveFromBox(subject.contained);
			contained.Remove(subject.gameObject);
			subject.OnRemovedFromBox();
			OnContentsUpdated?.Invoke();
		}
		UpdateShippable();
	}

	private void OnBoxContentsUpdated()
	{
		// The contents have changed, so check if all items in the box are still valid
		var toRemove = contained.Where(o => !body.Has(o)).ToList();
		foreach (var o in toRemove)
		{
			if (!o) continue;
			contained.Remove(o);
			var comp = o.GetComponent<BoxScript<Contained>>();
			if (comp)
			{
				Box.RemoveFromBox(comp.contained);
				comp.OnRemovedFromBox();
			}
		}
		OnContentsUpdated?.Invoke();
		UpdateShippable();
	}
}
