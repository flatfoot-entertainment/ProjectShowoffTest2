using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class BoxBody : MonoBehaviour
{
	/// <summary>
	/// Called when the contents of the body change
	/// </summary>
	public Action OnContentsUpdated;

	/// <summary>
	/// All the items currently intersect with the body
	/// </summary>
	public List<GameObject> intersecting { get; private set; } = new List<GameObject>();
	// A cache to calculate the next intersecting list

	private List<GameObject> nextList = new List<GameObject>();

	private void OnTriggerStay(Collider other)
	{
		nextList.Add(other.gameObject);
	}

	private void FixedUpdate()
	{
		// On the other hand, this is only realistically on one GameObject at a time
		
		// Check if lists differ
		// Either the count is different or their intersecting set is non-empty
		bool differing = intersecting.Count != nextList.Count;
		if (!differing) differing = intersecting.Except(nextList).Any();

		// Reset the lists
		intersecting.Clear();
		intersecting = nextList;
		nextList = new List<GameObject>();

		// If the contents change, call the callback for that
		if (differing) OnContentsUpdated?.Invoke();
	}

	/// <summary>
	/// Check if a <see cref="GameObject"/> is in the box
	/// </summary>
	/// <param name="item"></param>
	/// <returns></returns>
	public bool Has(GameObject item)
	{
		return intersecting.Contains(item);
	}
}
