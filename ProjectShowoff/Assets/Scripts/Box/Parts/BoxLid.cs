using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(Collider))]
public class BoxLid<Contained> : MonoBehaviour
{
	/// <summary>
	/// Callback that is called, when an object exits the lid
	/// </summary>
	public Action<BoxScript<Contained>> OnExitCallback;

	/// <summary>
	/// Callback that is called, when an object enters the lid
	/// </summary>
	public Action<BoxScript<Contained>> OnEnterCallback;

	/// <summary>
	/// A list of objects in the lid
	/// </summary>
	public List<BoxScript<Contained>> inLid { get; private set; } = new List<BoxScript<Contained>>();
	private List<BoxScript<Contained>> nextList = new List<BoxScript<Contained>>();

	private Collider coll;

	private void Start()
	{
		coll = GetComponent<Collider>();
	}

	private void OnTriggerStay(Collider other)
	{
		var comp = other.gameObject.GetComponent<BoxScript<Contained>>();
		if (comp) nextList.Add(comp);
	}

	private void FixedUpdate()
	{
		bool differing = inLid.Count != nextList.Count;
		if (!differing) differing = inLid.Except(nextList).Any();
		if (differing)
		{
			var old = inLid.Except(nextList).ToList();
			var n = nextList.Except(inLid).ToList();

			UpdateListsForNewFrame();

			// Call the On*Callbacks for each item that exited/entered
			foreach (BoxScript<Contained> b in n) OnEnterCallback(b);
			foreach (BoxScript<Contained> b in old) OnExitCallback(b);
		}
		else
			UpdateListsForNewFrame();
	}

	/// <summary>
	/// Swap the <see cref="inLid"/> and <see cref="nextList"/> lists
	/// </summary>
	private void UpdateListsForNewFrame()
	{
		inLid.Clear();
		inLid = nextList;
		nextList = new List<BoxScript<Contained>>();
	}

	/// <summary>
	/// Basically set the associated collider to not be a trigger anymore, to prevent items from flying out
	/// </summary>
	public void Close()
	{
		coll.isTrigger = false;
	}
}
