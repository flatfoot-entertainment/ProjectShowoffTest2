using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// An <see cref="IBox{Contained}"/>, specifically for <see cref="Item"/>s
/// </summary>
public class ItemBox : IBox<Item>
{
	/// <inheritdoc/>
	public int MoneyValue
	{
		get
		{
			int contentsSum = 0;
			foreach (ItemType itemType in BoxContents.Keys.ToList())
			{
				contentsSum += BoxContents[itemType];
			}

			return contentsSum;
		}
	}

	/// <summary>
	/// The items in this box, but sorted by type. Might be easier in some cases compared to <see cref="Contents"/>.
	/// </summary>
	public Dictionary<ItemType, int> BoxContents { get; } = new Dictionary<ItemType, int>();

	/// <inheritdoc/>
	public List<Item> Contents { get; } = new List<Item>();

	/// <inheritdoc/>
	public void AddToBox(Item contained)
	{
		if (!BoxContents.ContainsKey(contained.Type))
			BoxContents.Add(contained.Type, Item.Value);
		else
			BoxContents[contained.Type] = BoxContents[contained.Type] + Item.Value;
		Contents.Add(contained);
	}

	/// <inheritdoc/>
	public void RemoveFromBox(Item contained)
	{
		if (Contents.Contains(contained))
		{
			BoxContents[contained.Type] -= Item.Value;
			if (BoxContents[contained.Type] <= 0f)
			{
				BoxContents.Remove(contained.Type);
			}
			Contents.Remove(contained);
		}
	}
}
