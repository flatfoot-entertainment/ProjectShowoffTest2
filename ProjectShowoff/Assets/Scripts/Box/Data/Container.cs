using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Container : IBox<ItemBox>
{
	/// <inheritdoc/>
	public int MoneyValue
	{
		get
		{
			int val = 0;
			foreach (ItemBox box in Contents)
			{
				val += box.MoneyValue;
			}
			return val;
		}
	}

	/// <inheritdoc/>
	public List<ItemBox> Contents { get; private set; }

	public Container()
	{
		Contents = new List<ItemBox>();
	}

	/// <inheritdoc/>
	public void AddToBox(ItemBox contained)
	{
		if (!Contents.Contains(contained))
		{
			Contents.Add(contained);

			AddBoxToCached(contained);
		}
	}

	/// <inheritdoc/>
	public void RemoveFromBox(ItemBox contained)
	{
		Contents.Remove(contained);
		RemoveBoxFromCached(contained);
	}

	/// <summary>
	/// Add the contents of the box to the cached dictionary, which just exposes items
	/// </summary>
	private void AddBoxToCached(ItemBox box)
	{
		foreach (Item item in box.Contents)
		{
			if (Items.ContainsKey(item.Type))
				Items[item.Type] += Item.Value;
			else Items[item.Type] = Item.Value;
		}
	}

	/// <summary>
	/// Remove the contents of the box from the cached dictionary, which just exposes items
	/// </summary>
	private void RemoveBoxFromCached(ItemBox box)
	{
		foreach (Item item in box.Contents)
		{
			if (Items.ContainsKey(item.Type))
			{
				Items[item.Type] -= Item.Value;
				if (Items[item.Type] <= 0)
					Items.Remove(item.Type);
			}
		}
	}

	/// <summary>
	/// The <see cref="Item"/>s in this Container, skipping the step of going through the <see cref="ItemBox"/>es
	/// </summary>
	public Dictionary<ItemType, int> Items { get; } = new Dictionary<ItemType, int>();

	/// <summary>
	/// Add the contents of the two boxes
	/// </summary>
	/// <returns>A new box with the contents of the other two boxes</returns>
	public static Container operator +(Container a, Container b)
	{
		var ret = new Container
		{
			Contents = a.Contents.Concat(b.Contents).ToList()
		};
		foreach (var content in ret.Contents)
		{
			ret.AddBoxToCached(content);
		}
		
		return ret;
	}
}