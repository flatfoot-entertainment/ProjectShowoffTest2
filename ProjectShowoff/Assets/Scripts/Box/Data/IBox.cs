using System;
using System.Collections.Generic;

/// <summary>
/// A generic way of storing something in a box like way
/// </summary>
/// <typeparam name="Contained">The thing stored inside the box</typeparam>
public interface IBox<Contained>
{
	/// <summary>
	/// How much is the box worth [OBSOLETE]
	/// </summary>
	[Obsolete]
	int MoneyValue { get; }
	/// <summary>
	/// The things inside the box
	/// </summary>
	List<Contained> Contents { get; }

	/// <summary>
	/// Add something of the specified type to the box.
	/// </summary>
	/// <param name="contained">The thing to add</param>
	void AddToBox(Contained contained);

	/// <summary>
	/// Remove something of the specified type to the box.
	/// </summary>
	/// <param name="contained">The thing to remove</param>
	void RemoveFromBox(Contained contained);
}