﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class DestroyBoxOnDisable : MonoBehaviour
{
	[SerializeField] private LayerMask layerToDestroy;
	
	private BoxCollider coll;
	private BoxCollider Collider
	{
		get
		{
			if (!coll)
				coll = GetComponent<BoxCollider>();
			return coll;
		}
	}
	[SerializeField] private Vector3 sizeMultiplier;

#if UNITY_EDITOR
	private void OnDrawGizmos()
	{
		var oldMat = Gizmos.matrix;
		Matrix4x4 rot = Matrix4x4.Translate(transform.position + Collider.center) * Matrix4x4.Rotate(transform.rotation);
		Gizmos.matrix = rot;

		Gizmos.DrawWireCube(Vector3.zero, sizeMultiplier.ComponentMultiply(transform.lossyScale.ComponentMultiply(Collider.size)));

		Gizmos.matrix = oldMat;
	}
#endif

	private void OnDisable()
	{
		var toDestroy = Physics.OverlapBox(
			transform.position + Collider.center,
			sizeMultiplier.ComponentMultiply(transform.lossyScale.ComponentMultiply(Collider.size)) / 2f,
			transform.rotation,
			layerToDestroy
		);
		foreach (var c in toDestroy)
		{
			if (c && c.gameObject)
				Destroy(c.gameObject);
		}
	}
}
