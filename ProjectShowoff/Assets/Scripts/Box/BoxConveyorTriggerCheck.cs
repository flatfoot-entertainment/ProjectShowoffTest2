using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Used to check if box is on the box conveyor.
public class BoxConveyorTriggerCheck : MonoBehaviour
{
	[SerializeField] private LayerMask layerMask;
	[SerializeField] private bool isInsidePlacementVolume;
	[SerializeField, Layer] private int newLayer;

	//A placement volume to ensure box is within the bounds of the conveyor
	private void OnTriggerEnter(Collider other)
	{
		if (layerMask.Contains(other.gameObject.layer))
		{
			isInsidePlacementVolume = true;
		}
		else isInsidePlacementVolume = false;
	}
	private void OnTriggerExit(Collider other)
	{
		isInsidePlacementVolume = false;
	}

	private void OnCollisionEnter(Collision other)
	{
		if (!isInsidePlacementVolume || !layerMask.Contains(other.gameObject.layer)) return;
		EventScript.Handler.BroadcastEvent(new BoxConveyorPlaceEvent());
		EventScript.Handler.BroadcastEvent(new ManageButtonBlinkingEvent(EventType.ReturnToShippingButtonBlink, true));
		gameObject.layer = newLayer;
	}
}
