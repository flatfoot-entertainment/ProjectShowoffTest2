﻿using UnityEngine;

/// <summary>
/// A simple script used to hold the rotation of an item
/// </summary>
public class ItemRotationScript : MonoBehaviour
{
	public Vector3 boxRotation;
}
