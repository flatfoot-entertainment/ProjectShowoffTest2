using UnityEngine;

/// <summary>
/// A class that keeps track of the items inside of it and can be delivered (by being added to another box).
/// <br/>
/// It can only be delivered once.
/// </summary>
public class ShippableItemBox : MonoBehaviour
{
	// A callback for when this box is shipped
	public event System.Action OnShipment;

	public ItemBox Box { get; private set; }

	// A flag to prevent a box from being shipped twice
	private bool delivered;

	public void Init(ItemBox box)
	{
		delivered = false;
		Box = box;
		// Add an ItemBoxScript to know when it has been added to stuff
		ItemBoxScript data = gameObject.AddComponent<ItemBoxScript>();
		data.AddedToBoxCallback += Deliver;
		data.contained = Box;
	}

	// A function for subclasses to call when their specific requirements for shipment are fulfilled
	// (A trigger hit, etc.)
	private void Deliver()
	{
		if (delivered) return;
		OnShipment?.Invoke();
		delivered = true;
	}

	private void OnDestroy()
	{
		// Reset the callback for safety
		OnShipment = null;
	}
}