﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BoxContentsUI : MonoBehaviour
{
	[SerializeField] private RequirementUIContainer containerPrefab;
	private Dictionary<ItemType, RequirementUIContainer> containers = new Dictionary<ItemType, RequirementUIContainer>();
	
	public void UpdateContents(Dictionary<ItemType, int> newContents)
	{
		RemoveUnneededRequirementUIs(newContents);

		// Can't change a collection while iterating over it :/
		var lateAdd = new List<(ItemType, RequirementUIContainer)>();

		foreach (var itemType in newContents.Keys)
		{
			// Update the RequirementUIContainers, or instantiate new ones (whatever is necessary)
			if (containers.ContainsKey(itemType)) containers[itemType].UpdateContainer(newContents[itemType]);
			else
			{
				var container = Instantiate(containerPrefab, transform);
				container.SetupRequirementContainer(itemType, newContents[itemType]);
				lateAdd.Add((itemType, container));
			}
		}

		// Add all the RequirementUIContainers that should be added
		foreach (var (itemType, requirementUIContainer) in lateAdd)
		{
			containers.Add(itemType, requirementUIContainer);
		}
		
		// Order containers: I don't like this approach
		int index = 0;
		foreach (var itemType in Extensions.GetValues<ItemType>())
		{
			if (containers.ContainsKey(itemType))
			{
				containers[itemType].transform.SetSiblingIndex(index);
				index++;
			}
		}
	}

	/// <summary>
	/// Remove all of the <see cref="RequirementUIContainer"/>s, that aren't needed with the new contents
	/// </summary>
	private void RemoveUnneededRequirementUIs(Dictionary<ItemType, int> newContents)
	{
		var toRemove = containers.Keys.Except(newContents.Keys).ToArray();
		foreach (var itemType in toRemove)
		{
			Destroy(containers[itemType].gameObject);
			containers.Remove(itemType);
		}
	}

	/// <summary>
	/// Clear the display
	/// </summary>
	public void ClearUI()
	{
		foreach (var obj in containers.Values)
		{
			Destroy(obj.gameObject);
		}
		containers.Clear();
	}
}
