using UnityEngine;

//Used to link up the SimpleConveyor and ConveyorAnimatorManager
public class ConveyorSetupScript : MonoBehaviour
{
	/// <summary>
	/// All the item spawners associated with this conveyor
	/// </summary>
	public ItemSpawner[] ItemSpawners => itemSpawners;
	/// <summary>
	/// All the <see cref="SimpleConveyor"/>s associated with this conveyor
	/// </summary>
	public SimpleConveyor[] ConveyorScripts => conveyorScripts;
	/// <summary>
	/// All the <see cref="ConveyorAnimatorManager"/>s associated with this conveyor
	/// </summary>
	public ConveyorAnimatorManager[] Animators => animators;
	/// <summary>
	/// Initial speed of this conveyor
	/// </summary>
	[SerializeField] private float initialSpeed = 2.0f;
	[SerializeField] private SimpleConveyor[] conveyorScripts;
	[SerializeField] protected ItemSpawner[] itemSpawners;
	[SerializeField] private ConveyorAnimatorManager[] animators;

	// Start is called before the first frame update
	protected virtual void Awake()
	{
		conveyorScripts = GetComponentsInChildren<SimpleConveyor>();
		foreach (var conveyor in conveyorScripts) conveyor.InitialSpeed = initialSpeed;
		itemSpawners = GetComponentsInChildren<ItemSpawner>();
		animators = GetComponentsInChildren<ConveyorAnimatorManager>();
	}
}