using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorAnimatorManager : MonoBehaviour
{
	/// <summary>
	/// The reference speed of the animator. Any speed will be multiplied by this.
	/// </summary>
	[SerializeField] private float initialSpeed;
	/// <summary>
	/// The list of animators, this manager controls.
	/// <br/>
	/// This is filled in at start
	/// </summary>
	[SerializeField] private Animator[] animators;

	private bool isRunning;

	public float Speed
	{
		set
		{
			foreach (var animator in animators) animator.speed = initialSpeed * value;
		}
	}

	private void Start()
	{
		animators = GetComponentsInChildren<Animator>();
		Speed = 1f;
		EventScript.Handler.Subscribe(EventType.ManageConveyorAnimator, ManageAnimation);
	}

	private void OnValidate()
	{
		if (Application.isPlaying)
		{
			Speed = 1f;
		}
	}

	/// <summary>
	/// Call this enumerator to stop the animators for the specified time.
	/// </summary>
	/// <param name="time">The time to stop the conveyors for</param>
	public IEnumerator Stop(float time)
	{
		Speed = 0f;
		yield return new WaitForSeconds(time);
		Speed = 1f;
	}

	private void ManageAnimation(Event e)
	{
		if (!(e is ManageConveyorAnimatorEvent animEvent)) return;
		Speed = animEvent.SpeedModifier;
	}
}
