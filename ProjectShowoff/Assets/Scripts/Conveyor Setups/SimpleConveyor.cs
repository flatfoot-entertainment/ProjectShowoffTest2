using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script attached to every conveyor part
[RequireComponent(typeof(Rigidbody))]
public class SimpleConveyor : MonoBehaviour
{
	/// <summary>
	/// Set the speed to a multiple of the <see cref="InitialSpeed"/>.
	/// </summary>
	public float Speed
	{
		get => speed;
		set => speed = initialSpeed * value;
	}

	/// <summary>
	/// The initial speed of the conveyor
	/// </summary>
	public float InitialSpeed
	{
		get => initialSpeed;
		set => initialSpeed = value;
	}

	/// <summary>
	/// Is this conveyor currently stopped?
	/// </summary>
	public bool IsStopped { get; private set; }

	[SerializeField] private float speed;
	[SerializeField] private float initialSpeed = 2.0f;
	[SerializeField] private Vector3 moveDir;
	private Rigidbody rb;


	// Start is called before the first frame update
	private void Start()
	{
		speed = initialSpeed;
		rb = GetComponent<Rigidbody>();
	}

	private void FixedUpdate()
	{
		Vector3 oldPos = rb.position;
		rb.position += (-moveDir) * speed * Time.fixedDeltaTime;
		rb.MovePosition(oldPos);
	}

	/// <summary>
	/// Stop this conveyor for the specified amount of time.
	/// </summary>
	public IEnumerator Stop(float delay)
	{
		IsStopped = true;
		speed = 0f;
		yield return new WaitForSeconds(delay);
		Speed = 1f;
		IsStopped = false;
	}
}
