﻿

public class BoxUpgrade : Upgrade
{
	
	/// <inheritdoc />
	public BoxUpgrade(int pCost) : base(UpgradeType.BoxUpgrade, pCost)
	{
	}

	/// <inheritdoc />
	public override void ApplyUpgrade()
	{
		EventScript.Handler.BroadcastEvent(new BoxUpgradeEvent(Level));
	}

	/// <inheritdoc />
	public override void IncreaseLevel(int newCost)
	{
		if (Level <= 2)
		{
			base.IncreaseLevel(newCost);
		}
	}
}