using System;
using System.Collections.Generic;
using Config;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpgradeContainer : MonoBehaviour
{
	[SerializeField] private UpgradeSettings settings;
	[SerializeField] private UpgradeType upgradeType;
	[SerializeField] private Image image;
	[SerializeField] private TextMeshProUGUI levelText;
	[SerializeField] private TextMeshProUGUI costText;
	[SerializeField] private Button buyButton;

	[SerializeField] private string levelFormat = "Lv {0}";
	[SerializeField] private string costFormat = "{0}";
	[SerializeField] private string allStagesBoughtText = "DONE";

	private bool doneUpgrading;
	private bool hasPassedPrompt;

	private Upgrade upgrade;

	private float smallestPrice;
	void Awake()
	{
		upgrade = upgradeType switch
		{
			UpgradeType.ConveyorUpgrade => new ConveyorPartUpgrade(GetLevel(0)?.price ?? 0),
			UpgradeType.ConveyorStop => new ConveyorStop(GetLevel(0)?.price ?? 0),
			UpgradeType.BoxUpgrade => new BoxUpgrade(GetLevel(0)?.price ?? 0),
			_ => throw new ArgumentOutOfRangeException()
		};

		UpdateUI(GetLevel(0)?.upgradeImage);
		buyButton.interactable = true;
		EventScript.Handler.Subscribe(EventType.ContinueAfterPrompt, OnPromptShown);
		smallestPrice = GameHandler.Instance.GetComponent<ChangeGameState>().MinimalAmountOfMoney;
	}

	private void Update()
	{
		if (!isActiveAndEnabled) return;
		// Yes, I know this isn't the best in Update, but it's the fastest to do, so... :^)
		buyButton.interactable = !doneUpgrading && GameHandler.Instance.Money >= upgrade.Cost;
	}

	private void OnPromptShown(Event e)
	{
		ContinueAfterPromptEvent promptEvent = e as ContinueAfterPromptEvent;
		hasPassedPrompt = promptEvent.HasPassedPrompt;
		if (promptEvent.UpgradeType == upgradeType)
		{
			UpdateUpgrade();
		}
		else
		{
			Debug.LogError("probably just checking with other upgrades, i think its fine for now");
		}
	}

	public void UpdateUpgrade() //such name very read
	{
		if (doneUpgrading || !(upgrade.Level < OwnUpgrades().Count - 1))
		{
			doneUpgrading = true;
			buyButton.interactable = false;
			return;
		}

		if (GameHandler.Instance.Money < upgrade.Cost) return;
		if (!hasPassedPrompt && GameHandler.Instance.Money - upgrade.Cost < smallestPrice)
		{
			bool cannotContinue = GameHandler.Instance.GetComponent<ChangeGameState>().CannotContinue;
			Debug.Log($"CAN YOU BUY THE FUCKING UPGRADE WITHOUT LOSING?????{cannotContinue}");
			EventScript.Handler.BroadcastEvent(new ManageConfirmationPanelEvent(upgrade.Type, cannotContinue));
			return;
		}

		var newLevel = GetLevel(upgrade.Level + 1);
		if (!newLevel.buyable)
		{
			doneUpgrading = true;
			buyButton.interactable = false;
		}

		// This doesn't have to be a ManageUpgradeEvent -> This is clearer imo
		EventScript.Handler.BroadcastEvent(new ManageMoneyEvent(-upgrade.Cost));
		EventScript.Handler.BroadcastEvent(new SwitchShipEvent(++GameHandler.Instance.UpgradeIndex));
		upgrade.IncreaseLevel(newLevel.price);
		upgrade.ApplyUpgrade();

		UpdateUI(newLevel.upgradeImage);
	}

	private void UpdateUI(Sprite img)
	{
		if (img == null) return;
		levelText.text = string.Format(levelFormat, upgrade.Level.ToString());
		costText.text = doneUpgrading ? allStagesBoughtText : string.Format(costFormat, upgrade.Cost.ToString());
		image.sprite = img;
	}

	private UpgradeSettings.UpgradeLevel GetLevel(int index)
	{
		var def = OwnUpgradeAt(index);
		var thing = ConfigLoader.GameConfig?.UpgradeSettings;
		if (thing == null) return def;
		var settingList = upgradeType switch
		{
			UpgradeType.ConveyorStop => thing.StopButtonUpgrades,
			UpgradeType.ConveyorUpgrade => thing.ConveyorUpgrades,
			UpgradeType.BoxUpgrade => thing.BoxUpgrades,
			_ => throw new ArgumentOutOfRangeException(nameof(upgradeType), upgradeType, null)
		};
		UpgradeSetting level = null;
		if (index < settingList.Count && index >= 0)
			level = settingList[index];
		// var level = settingList.Count > index ? settingList[index] : null;
		return level != null
			? new UpgradeSettings.UpgradeLevel
			{
				buyable = level.Buyable,
				price = level.Price,
				upgradeImage = def?.upgradeImage
			}
			: def;
	}

	private UpgradeSettings.UpgradeLevel OwnUpgradeAt(int index)
	{
		var own = OwnUpgrades();
		if (own.Count < index || index < 0)
		{
			var temp = own[own.Count - 1];
			return new UpgradeSettings.UpgradeLevel
			{
				price = temp.price,
				buyable = false,
				upgradeImage = temp.upgradeImage
			};
		}
		else if (index >= 0 && index < own.Count)
		{
			return own[index];
		}
		else
		{
			return null;
		}
	}

	private List<UpgradeSettings.UpgradeLevel> OwnUpgrades()
	{
		// WTF is this unholy construct???
		return upgradeType switch
		{
			UpgradeType.ConveyorStop => settings.stopButtonLevels,
			UpgradeType.ConveyorUpgrade => settings.conveyorUpgrades,
			UpgradeType.BoxUpgrade => settings.boxUpgrades,
			_ => null
		};
	}

}
