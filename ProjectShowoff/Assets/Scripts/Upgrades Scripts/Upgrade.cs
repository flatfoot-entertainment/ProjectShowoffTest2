using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum UpgradeType
{
	ConveyorStop,
	ConveyorUpgrade,
	BoxUpgrade
}

public abstract class Upgrade
{
	public UpgradeType Type => type;
	public int Level => level;
	public int Cost => cost;

	private readonly UpgradeType type;
	private int level;
	private int cost;

	public Upgrade(UpgradeType pType, int pCost)
	{
		level = 0;
		type = pType;
		cost = pCost;
	}

	public abstract void ApplyUpgrade();

	public virtual void IncreaseLevel(int newCost)
	{
		level++;
		cost = newCost;
	}

	public void ResetLevel()
	{
		level = 0;
	}
}
