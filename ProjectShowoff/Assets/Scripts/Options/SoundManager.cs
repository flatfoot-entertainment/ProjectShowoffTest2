
public sealed class SoundManager
{
	/// <summary>
	/// The sfx volume in the game
	/// </summary>
	public float SfxVolume { get; set; }

	/// <summary>
	/// The music volume in the game
	/// </summary>
	public float MusicVolume { get; set; }

	public static SoundManager Instance =>
		instance ??= new SoundManager
		{
			SfxVolume = 1f,
			MusicVolume = 1f
		};

	private static SoundManager instance;
}
