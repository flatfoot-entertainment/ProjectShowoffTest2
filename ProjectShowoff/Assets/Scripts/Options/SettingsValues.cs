using UnityEngine;
using UnityEngine.UI;

public class SettingsValues : MonoBehaviour
{
	[SerializeField] private Slider musicSlider, sfxSlider;
	private void OnEnable()
	{
		musicSlider.value = SoundManager.Instance.MusicVolume;
		sfxSlider.value = SoundManager.Instance.SfxVolume;
	}
}
