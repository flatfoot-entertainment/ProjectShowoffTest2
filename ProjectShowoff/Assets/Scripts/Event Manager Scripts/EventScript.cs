using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventScript
{
	private static EventScript Instance => instance ??= new EventScript();
	private static EventScript instance;
	public static EventManager Handler => Instance.eventManager;

	private EventManager eventManager;
	// Start is called before the first frame update

	private EventScript()
	{
		eventManager = new EventManager();
	}
}
