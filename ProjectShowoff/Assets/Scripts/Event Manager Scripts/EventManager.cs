using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void EventHandler(Event e);
public class EventManager
{
	private Dictionary<EventType, EventHandler> subscribers;

	public EventManager()
	{
		subscribers = new Dictionary<EventType, EventHandler>();
	}

	/// <summary>
	/// Subscribe to a specific event. You will receive a callback with the event in it once it's raised.
	/// </summary>
	public void Subscribe(EventType eventType, EventHandler eventHandler)
	{
		if (!subscribers.ContainsKey(eventType))
		{
			EventHandler handler = null;
			subscribers.Add(eventType, handler);
		}
		subscribers[eventType] += eventHandler;
	}

	/// <summary>
	/// Broadcast an event, so subscribers get notified
	/// </summary>
	public void BroadcastEvent(Event e)
	{
		if (subscribers.ContainsKey(e.type))
		{
			subscribers[e.type]?.Invoke(e);
		}
	}

	/// <summary>
	/// Force unsubscribe all subscribers
	/// </summary>
	public void UnsubscribeAll()
	{
		subscribers.Clear();
	}
}
