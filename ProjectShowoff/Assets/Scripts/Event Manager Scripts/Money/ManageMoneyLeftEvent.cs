using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Used for checking how much money is left (win/lose condition)
public class ManageMoneyLeftEvent : Event
{
	public ManageMoneyLeftEvent() : base(EventType.ManageMoneyLeft)
	{

	}
}
