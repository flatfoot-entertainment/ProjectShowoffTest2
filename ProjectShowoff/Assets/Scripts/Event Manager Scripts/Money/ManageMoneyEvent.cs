using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Main event class that is used for handling money in several situations (buying boxes, planets, upgrades etc.)
public class ManageMoneyEvent : Event
{
	public int Amount => amount;
	private int amount;

	public ManageMoneyEvent(int pAmount) : base(EventType.ManageMoney)
	{
		amount = pAmount;
	}
}
