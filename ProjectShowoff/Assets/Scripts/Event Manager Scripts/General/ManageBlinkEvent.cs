using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Class for managing the warning event image shown (probably needs renaming)
public class ManageBlinkEvent : Event
{
	public RandomEvent RandomEvent
	{
		get => randomEvent;
		set => randomEvent = value;
	}
	private RandomEvent randomEvent;

	public ManageBlinkEvent(RandomEvent pRandomEvent) : base(EventType.ManageBlinkEvent)
	{
		randomEvent = pRandomEvent;
	}

}
