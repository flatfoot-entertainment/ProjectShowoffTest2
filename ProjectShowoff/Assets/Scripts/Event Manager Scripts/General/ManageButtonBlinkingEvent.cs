using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//General class for handling buttons blinking after something happens.
public class ManageButtonBlinkingEvent : Event
{
	public bool IsBlinking => isBlinking;
	private bool isBlinking;

	public EventType Affected { get; }

	public ManageButtonBlinkingEvent(EventType pEventType, bool pIsBlinking) : base(EventType.ButtonBlinkEvent)
	{
		Affected = pEventType;
		isBlinking = pIsBlinking;
	}
}
