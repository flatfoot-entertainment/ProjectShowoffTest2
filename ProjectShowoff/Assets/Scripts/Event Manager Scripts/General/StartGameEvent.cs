﻿using UnityEngine;

public class StartGameEvent : Event
{
	/// <inheritdoc />
	public StartGameEvent() : base(EventType.StartGame)
	{
	}
}
