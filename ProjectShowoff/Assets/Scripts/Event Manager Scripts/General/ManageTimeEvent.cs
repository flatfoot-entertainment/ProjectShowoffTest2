using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Used for saving time at the end of the game (probably to be replaced with highscores)
public class ManageTimeEvent : Event
{
	public float TimeAmount => timeAmount;
	private float timeAmount;
	public ManageTimeEvent(float pTimeAmount) : base(EventType.ManageTime)
	{
		timeAmount = pTimeAmount;
	}
}
