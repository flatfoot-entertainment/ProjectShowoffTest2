using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Used for swapping ship model based on upgrades bought
public class SwitchShipEvent : Event
{
	public int Index => index;
	private int index;
	public SwitchShipEvent(int pIndex) : base(EventType.SwitchShip)
	{
		index = pIndex;
	}
}
