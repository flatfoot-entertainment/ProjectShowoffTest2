﻿using UnityEngine;

public class OrderCompleteEvent : Event
{
	/// <inheritdoc />
	public OrderCompleteEvent() : base(EventType.OrderComplete)
	{
	}
}
