using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Used for handling when the warning panel appears (if you don't have enough cash from upgrading)
public class ManageConfirmationPanelEvent : Event
{
	public UpgradeType UpgradeType => upgradeType;
	public bool CanPurchase => canPurchase;
	private UpgradeType upgradeType;
	private bool canPurchase;
	public ManageConfirmationPanelEvent(UpgradeType pUpgradeType, bool pCanPurchase) : base(EventType.ManageConfirmationPanel)
	{
		upgradeType = pUpgradeType;
		canPurchase = pCanPurchase;
	}
}
