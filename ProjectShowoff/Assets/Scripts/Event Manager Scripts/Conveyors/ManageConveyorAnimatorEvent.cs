using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Self explanatory.
public class ManageConveyorAnimatorEvent : Event
{
	public float SpeedModifier { get; }

	public ManageConveyorAnimatorEvent(float speedModifier) : base(EventType.ManageConveyorAnimator)
	{
		SpeedModifier = speedModifier;
	}
}
