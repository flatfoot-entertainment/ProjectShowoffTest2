using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Used for stuff when a box is placed on conveyor
public class BoxConveyorPlaceEvent : Event
{
	public BoxConveyorPlaceEvent() : base(EventType.BoxConveyorPlace)
	{

	}
}
