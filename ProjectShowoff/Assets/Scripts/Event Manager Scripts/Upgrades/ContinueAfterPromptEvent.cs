using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinueAfterPromptEvent : Event
{
	public bool HasPassedPrompt => hasPassedPrompt;
	public UpgradeType UpgradeType => upgradeType;
	private bool hasPassedPrompt;
	private UpgradeType upgradeType;
	public ContinueAfterPromptEvent(bool pHasPassedPrompt, UpgradeType pUpgradeType) : base(EventType.ContinueAfterPrompt)
	{
		hasPassedPrompt = pHasPassedPrompt;
		upgradeType = pUpgradeType;
	}
}
