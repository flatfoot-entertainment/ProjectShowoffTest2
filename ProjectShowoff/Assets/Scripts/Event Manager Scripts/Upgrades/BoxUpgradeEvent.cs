﻿// ${DATE} because Rider is ass

public class BoxUpgradeEvent : Event
{
	public int Level { get; }

	/// <inheritdoc />
	public BoxUpgradeEvent(int newLevel) : base(EventType.BoxUpgrade)
	{
		Level = newLevel;
	}
}