using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum EventType
{
	ManageMoney,
	ManageMoneyLeft,
	ManageUpgrade,
	ManageConfirmationPanel,
	ManageTime,
	ManageConveyors,
	ManageBoxSelect,
	ManageBlinkEvent,
	ManagePlanetRequirement,
	ManageConveyorAnimator,
	ConveyorUpgrade,
	ConveyorStopButtonUpgrade,
	ContinueAfterPrompt,
	BoxUpgrade,
	BoxClose,
	BoxConveyorPlace,
	BoxBuy,
	CameraMove,
	OrderComplete,
	StartGame,
	SwitchShip,
	ButtonBlinkEvent,
	ReturnToPackagingButtonBlink,
	ReturnToShippingButtonBlink,
	UpgradeButtonBlink,
	ShipButtonBlink,

}
public abstract class Event
{
	public readonly EventType type;

	public Event(EventType pType)
	{
		type = pType;
	}
}
