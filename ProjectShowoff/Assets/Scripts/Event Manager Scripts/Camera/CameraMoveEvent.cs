

//Linked to CameraEventRaiser
public class CameraMoveEvent : Event
{
	[System.Serializable]
	public enum CameraState
	{
		MainMenu,
		Packaging,
		Shipping
	}

	public CameraState NewState { get; }

	public CameraMoveEvent(CameraState state) : base(EventType.CameraMove)
	{
		NewState = state;
	}
}