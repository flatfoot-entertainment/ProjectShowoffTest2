﻿using UnityEngine;


//Used for changing to another vCam based on CameraState
public class CameraEventRaiser : MonoBehaviour
{
	[SerializeField] private CameraMoveEvent.CameraState switchTo;

	public void Raise()
	{
		EventScript.Handler.BroadcastEvent(new CameraMoveEvent(switchTo));
	}
}
