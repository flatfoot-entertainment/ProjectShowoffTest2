﻿using System;
using UnityEngine;
using System.IO;

public class GitHashLoader
{
	// ??= looks cool xD
	private static GitHashLoader Instance => instance ??= new GitHashLoader();
	private static GitHashLoader instance;

	/// <summary>
	/// The long hash of the last commit at the point of building.
	/// <br/>
	/// If file doesn't exist: <c>UNKNOWN</c>
	/// <br/>
	/// If error while loading: <c>ERROR</c>
	/// </summary>
	public static string Hash => Instance.hash;
	private readonly string hash;

	/// <summary>
	/// The short hash of the last commit at the point of building.
	/// <br/>
	/// If file doesn't exist: <c>UNKNOWN</c>
	/// <br/>
	/// If error while loading: <c>ERROR</c>
	/// </summary>
	public static string ShortHash => Instance.shortHash;
	private readonly string shortHash;

	private static string Path => System.IO.Path.Combine(Application.streamingAssetsPath, "git-info.txt");
	
	private GitHashLoader()
	{
		try
		{
			if (File.Exists(Path))
			{
				var reader = File.OpenText(Path);
				hash = reader.ReadLine();
				shortHash = reader.ReadLine();
			}
			else
			{
				hash = "UNKNOWN";
				shortHash = "UNKNOWN";
			}
		}
		catch (Exception)
		{
			hash = "ERROR";
			shortHash = "ERROR";
		}
	}
}