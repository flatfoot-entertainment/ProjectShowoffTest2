﻿using System;
using TMPro;
using UnityEngine;

public class GitHashInfoDisplay : MonoBehaviour
{
	[Serializable]
	public enum DisplayMode
	{
		Long,
		Short,
		Both
	}

	[SerializeField, Tooltip("The text to fill")] private TextMeshProUGUI text;
	[SerializeField, Tooltip("Should both hashes be shown or only the long/short one?")] private DisplayMode mode;
	[SerializeField, Tooltip("The string to format the hash(es) with")] private string format = "{0}";

	private void Start()
	{
		text.text = mode switch
		{
			DisplayMode.Long => string.Format(format, GitHashLoader.Hash),
			DisplayMode.Short => string.Format(format, GitHashLoader.ShortHash),
			DisplayMode.Both => string.Format(format, GitHashLoader.Hash, GitHashLoader.ShortHash),
			_ => throw new ArgumentOutOfRangeException()
		};
	}
}
