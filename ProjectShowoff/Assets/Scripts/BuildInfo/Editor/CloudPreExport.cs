﻿// ${DATE} because Rider is ass

#if UNITY_CLOUD_BUILD

using System.IO;
using UnityEngine;

public class CloudPreExport
{
	public static void PreExport(UnityEngine.CloudBuild.BuildManifestObject manifest)
	{
		// Get the commit id from the BuildManifestObject and write it to the file
		string hash = manifest.GetValue("scmCommitId", "unknown");
		// The short hash is just the first 7 characters of the long hash
		string shortHash = hash.Substring(0, 7);
		string path = Path.Combine(Application.streamingAssetsPath, "git-info.txt");
		Debug.Log($"Writing hash info to {path}");
		StreamWriter file = File.CreateText(path);
		file.WriteLine(hash);
		file.WriteLine(shortHash);
		file.Close();
	}
}

#endif
