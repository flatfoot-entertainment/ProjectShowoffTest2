﻿using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class BuildPreprocessor : IPreprocessBuildWithReport
{
	/// <inheritdoc />
	public int callbackOrder => 0;

	/// <inheritdoc />
	public void OnPreprocessBuild(BuildReport report)
	{
#if UNITY_CLOUD_BUILD
		Debug.Log("Getting Git commit hash is done in another function...");
#elif UNITY_EDITOR_WIN
		// Get the short and long Git hashes
		Debug.Log("Extracting and saving Git commit hash...");
		string hash = GetCurrentGitHash();
		Debug.Log($"Found commit hash \"{hash}\"");
		string shortHash = GetCurrentGitHash(true);
		Debug.Log($"Found short commit hash \"{shortHash}\"");
		
		// Save both of them to the simplest file type imaginable
		string path = Path.Combine(Application.streamingAssetsPath, "git-info.txt");
		Debug.Log($"Writing hash info to {path}");
		StreamWriter file = File.CreateText(path);
		file.WriteLine(hash);
		file.WriteLine(shortHash);
		file.Close();
#else
		Debug.Log("Getting the Git commit hash is not supported on this platform");
#endif
	}

	/// <summary>
	/// Get the current Git hash as a string, using a CMD invocation
	/// </summary>
	/// <param name="shortHash">Should the commit be the short hash</param>
	/// <returns>The short or long Git hash of the last commit</returns>
	private static string GetCurrentGitHash(bool shortHash = false)
	{
#if UNITY_EDITOR_WIN
		// Define the process
		var gitProc = new ProcessStartInfo
		{
			UseShellExecute = false,
			// Set the working directory to be the project root path
			WorkingDirectory = Application.dataPath + "/../",
			// Use CMD -> Sadly only windows :(
			FileName = @"C:\Windows\System32\cmd.exe",
			// The command to execute
			Arguments = $"/c git rev-parse {(shortHash ? "--short " : "")}HEAD",
			WindowStyle = ProcessWindowStyle.Hidden,
			RedirectStandardOutput = true,
			RedirectStandardError = true,
			CreateNoWindow = true
		};

		// Start the process
		var proc = Process.Start(gitProc);
		// If the process is null, it couldn't start
		if (proc == null)
		{
			Debug.LogError("Could not start git rev-parse command");
			return null;
		}

		// Wait for the process to finish
		proc.WaitForExit();
		// Read the output
		string output = proc.StandardOutput.ReadToEnd();
		// A non-zero exit code is a failure
		if (proc.ExitCode != 0)
		{
			string error = proc.StandardError.ReadToEnd();
			string msg = "" + (error.Length > 0 ? $"Standard Error:\n{error}\n" : "")
			                + (output.Length > 0 ? $"Standard Output:\n{output}" : "");
			Debug.LogError($"Could not complete git rev-parse command.\nMSG:\n{msg}");
			return null;
		}
		return output.Trim();
#else
		return "Unsupported Build Platform!";
#endif
	}
}