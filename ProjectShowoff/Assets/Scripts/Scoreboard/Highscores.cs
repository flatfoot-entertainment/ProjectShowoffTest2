﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

public class Highscores
{
	public class Entry : IComparable<Entry>, IComparable
	{
		/// <summary>
		/// The time this play-through took
		/// </summary>
		public readonly float time;
		/// <summary>
		/// The name of the lucky high-score holder
		/// </summary>
		public readonly string name;

		/// <inheritdoc />
		public int CompareTo(Entry other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;
			var timeComparison = time.CompareTo(other.time);
			return timeComparison != 0 
				? timeComparison
				: string.Compare(name, other.name, StringComparison.CurrentCulture);
		}

		/// <inheritdoc />
		public int CompareTo(object obj)
		{
			if (ReferenceEquals(null, obj)) return 1;
			if (ReferenceEquals(this, obj)) return 0;
			return obj is Entry other ? CompareTo(other) : throw new ArgumentException($"Object must be of type {nameof(Entry)}");
		}

		public static bool operator <(Entry left, Entry right)
		{
			return Comparer<Entry>.Default.Compare(left, right) < 0;
		}

		public static bool operator >(Entry left, Entry right)
		{
			return Comparer<Entry>.Default.Compare(left, right) > 0;
		}

		public static bool operator <=(Entry left, Entry right)
		{
			return Comparer<Entry>.Default.Compare(left, right) <= 0;
		}

		public static bool operator >=(Entry left, Entry right)
		{
			return Comparer<Entry>.Default.Compare(left, right) >= 0;
		}

		public Entry(string name, float time)
		{
			this.time = time;
			this.name = name;
		}
	}

	/// <summary>
	/// How many highscores are saved in this list at the most
	/// </summary>
	public static int HighscoreLimit
	{
		set
		{
			highscoreLimit = value;
			FitHighscores();
		}
	}
	private static int highscoreLimit;

	private static Highscores Instance => instance ??= new Highscores();
	private static Highscores instance;

	/// <summary>
	/// Get a collection of the highscores
	/// </summary>
	public static ReadOnlyCollection<Entry> Get => Instance.highscores.AsReadOnly();
	private List<Entry> highscores;

	private Highscores()
	{
		highscores = new List<Entry>();
	}

	/// <summary>
	/// Add a highscore to the handler by providing the time taken.
	/// </summary>
	/// <param name="time">The time taken by the player</param>
	/// <returns>The index of the highscore</returns>
	public static int AddEntry(string name, float time)
	{
		return AddEntry(new Entry(name, time));
	}

	/// <summary>
	/// Add a highscore to the handler by providing an <see cref="Entry"/>.
	/// </summary>
	/// <param name="e">The <see cref="Entry"/> object</param>
	/// <returns>The index of the highscore</returns>
	public static int AddEntry(Entry e)
	{
		Instance.highscores.Add(e);
		Instance.highscores.Sort();
		FitHighscores();
		return Instance.highscores.IndexOf(e);
	}

	private static void FitHighscores()
	{
		Instance.highscores = Instance.highscores.Take(highscoreLimit).ToList();
	}

	/// <summary>
	/// Check if the specified time would be a new high-score
	/// </summary>
	public static bool TimeIsHighscore(float time)
	{
		return Instance.highscores.Count <= 0 || Instance.highscores.Any(highscore => time < highscore.time);
	}
}
