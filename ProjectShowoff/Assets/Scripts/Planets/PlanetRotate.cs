using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRotate : MonoBehaviour
{
	[SerializeField] private float speed;
	private Vector3 axis;

	private void Start()
	{
		axis = Random.onUnitSphere;
	}

	// Update is called once per frame
	private void Update()
	{
		transform.RotateAround(transform.position, axis, speed * Time.deltaTime);
	}
}
