using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

public static class Extensions
{
	/// <summary>
	/// Get a random enum value
	/// </summary>
	public static T RandomEnumValue<T>() where T : Enum
	{
		var v = Enum.GetValues(typeof(T));
		return (T)v.GetValue(UnityEngine.Random.Range(0, v.Length));
	}

	/// <summary>
	/// Get a random element of this array
	/// </summary>
	public static T Random<T>(this T[] arr)
	{
		return arr[UnityEngine.Random.Range(0, arr.Length)];
	}

	/// <summary>
	/// Get a random element of this array with an index less than the specified one
	/// </summary>
	public static T Random<T>(this T[] arr, int maxIndex)
	{
		return arr[UnityEngine.Random.Range(0, arr.Length < maxIndex ? arr.Length : maxIndex)];
	}

	/// <summary>
	/// Get a random element of this list
	/// </summary>
	public static T Random<T>(this List<T> arr)
	{
		return arr[UnityEngine.Random.Range(0, arr.Count)];
	}

	/// <summary>
	/// Get a random element of this list with an index less than the specified one
	/// </summary>
	public static T Random<T>(this List<T> arr, int maxIndex)
	{
		return arr[UnityEngine.Random.Range(0, arr.Count < maxIndex ? arr.Count : maxIndex)];
	}

	/// <summary>
	/// Check if a <see cref="LayerMask"/> contains the specified layer
	/// </summary>
	public static bool Contains(this LayerMask mask, int layer)
	{
		return mask == (mask | (1 << layer));
	}

	/// <summary>
	/// Convert this dictionary into a more beautiful string representation than the default one
	/// </summary>
	public static string ToBeautifulString<T>(this Dictionary<ItemType, T> val)
	{
		string ret = "";
		foreach (var kvp in val)
		{
			ret += $"{kvp.Key.ToString()}: {kvp.Value}\n";
		}
		return ret;
	}

	/// <summary>
	/// Get a component of the <see cref="GameObject"/>, or if it doesn't exist add a new one and return it.
	/// </summary>
	public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
	{
		T comp = gameObject.GetComponent<T>();
		return comp ? comp : gameObject.AddComponent<T>();
	}

	/// <summary>
	/// Get the value out of the dictionary by key, or if the key is not in the dictionary, return the specified default.
	/// </summary>
	public static U ValueOr<T, U>(this Dictionary<T, U> dict, T key, U defaultValue)
	{
		return dict.ContainsKey(key) ? dict[key] : defaultValue;
	}

	/// <summary>
	/// Get the length of a list inside the dictionary with value type list. If the specified key doesn't exist in the
	/// dictionary, return 0
	/// </summary>
	public static int LengthOf<T, U>(this Dictionary<T, List<U>> dict, T key)
	{
		return dict.ContainsKey(key) && dict[key] != null ? dict[key].Count : 0;
	}

	/// <summary>
	/// See <see cref="LengthOf{T,U}(System.Collections.Generic.Dictionary{T,System.Collections.Generic.List{U}},T)"/>.
	/// <br/>
	/// This is the same, but for concurrent data structures.
	/// </summary>
	public static int LengthOf<T, U>(this ConcurrentDictionary<T, ConcurrentBag<U>> dict, T key)
	{
		return dict.ContainsKey(key) && dict[key] != null ? dict[key].Count : 0;
	}

	/// <summary>
	/// Set the x component of a vector
	/// </summary>
	public static void SetX(this Vector3 v, float val)
	{
		v.x = val;
	}

	/// <summary>
	/// Set the y component of a vector
	/// </summary>
	public static void SetY(this Vector3 v, float val)
	{
		v.y = val;
	}

	/// <summary>
	/// Set the z component of a vector
	/// </summary>
	public static void SetZ(this Vector3 v, float val)
	{
		v.z = val;
	}

	/// <summary>
	/// Get a random float between v.x and v.y.
	/// If v.x > v.y the float is between v.y and v.x.
	/// </summary>
	public static float Random(this Vector2 v)
	{
		return v.x < v.y 
			? UnityEngine.Random.Range(v.x, v.y)
			: UnityEngine.Random.Range(v.y, v.x);
	}

	/// <summary>
	/// Get a random int between v.x and v.y.
	/// /// If v.x > v.y the float is between v.y and v.x.
	/// </summary>
	public static int Random(this Vector2Int v)
	{
		return v.x < v.y
			? UnityEngine.Random.Range(v.x, v.y) 
			: UnityEngine.Random.Range(v.y, v.x);
	}

	/// <summary>
	/// Get an enumerator over the values of an enum
	/// </summary>
	public static IEnumerable<T> GetValues<T>() where T: Enum
	{
		return Enum.GetValues(typeof(T)).Cast<T>();
	}

	/// <summary>
	/// Multiply the components of two vectors one by one and return the result in a new vector
	/// </summary>
	public static Vector3 ComponentMultiply(this Vector3 a, Vector3 b)
	{
		return new Vector3
		{
			x = a.x * b.x,
			y = a.y * b.y,
			z = a.z * b.z,
		};
	}
}