using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabScroller : MonoBehaviour
{
	//i want it to cycle through all the item prefabs of the game
	//after the duration i want to set the public flicker speed and intensity of the hologram material to a low value, so it flickers fast and almost disappears, so we can transition to another item to be showed
	//the floating items should rotate
	//the 4 item types all have a different coloured hologram. 4 hologram materials are ready, but is it a better solution to have script change the colour value of 1 used material

	//time before it scrolls through to the next item
	public float Duration = 4;
	//the speed of the item rotation
	public float rotationSpeed = 2;


	//i assume we need unscaled time again for outside of the game because of the public duration
	private Renderer ren;

	Vector3 currentEulerAngles;
	float x;
	float y;
	float z;



	private void Start()
	{
		ren = GetComponent<Renderer>();
	}

	private void Update()
	{
		ren.sharedMaterial.SetFloat("_UnscaledTime", Time.unscaledTime);


		y = rotationSpeed - y;

		//modifying the Vector3, based on input multiplied by speed and time
		currentEulerAngles += new Vector3(x, y, z);

		//apply the change to the gameObject
		transform.eulerAngles = currentEulerAngles;

	}


	private void InstantiateItem()
	{
		
	}

	public void Spawn()
	{

		InstantiateItem();

	}


}


