using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeUIScript : MonoBehaviour
{
	[SerializeField] private CameraMoveEvent.CameraState stateA;
	[SerializeField] private CameraMoveEvent.CameraState stateB;
	[SerializeField] private CameraMoveEvent.CameraState stateC;

	[SerializeField] private GameObject[] stateAGameObjects;
	[SerializeField] private GameObject[] stateBGameObjects;

	private void Start()
	{
		EventScript.Handler.Subscribe(EventType.CameraMove, (e) =>
		{
			OnCamMove(((CameraMoveEvent)e).NewState);
		});
	}

	private void OnCamMove(CameraMoveEvent.CameraState newState)
	{
		if (!isActiveAndEnabled) return;
		if (newState == stateA)
		{
			StartCoroutine(UIEnableCoroutine(stateAGameObjects, stateBGameObjects));
		}
		else if (newState == stateB)
		{
			StartCoroutine(UIEnableCoroutine(stateBGameObjects, stateAGameObjects));
			//EventScript.Handler.BroadcastEvent(new ShipButtonBlinkEvent(false));
		}
		else if (newState == stateC)
		{
			foreach (GameObject gO in stateBGameObjects) gO.SetActive(false);
			foreach (GameObject gO in stateAGameObjects) gO.SetActive(false);
		}
	}

	private IEnumerator UIEnableCoroutine(GameObject[] gameObjectsToEnable, GameObject[] gameObjectsToDisable)
	{
		foreach (GameObject gO in gameObjectsToDisable) gO.SetActive(false);
		yield return new WaitForSeconds(2f);
		foreach (GameObject go in gameObjectsToEnable) go.SetActive(true);
	}
}
