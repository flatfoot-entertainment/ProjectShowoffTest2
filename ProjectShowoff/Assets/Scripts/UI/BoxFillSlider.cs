﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BoxFillSlider : MonoBehaviour
{
	[SerializeField] private RectTransform sliderSizeReference;
	[SerializeField] private RectTransform noProfitBackground;
	[SerializeField] private RectTransform profitBackground;
	[SerializeField] private RectTransform noProfitForeground;
	[SerializeField] private RectTransform profitForeground;
	[SerializeField] private Image overlayImage;
	[SerializeField] private GameObject overlayObject;

	[SerializeField] private TextMeshProUGUI profitText;
	[SerializeField] private Color lossColor;
	[SerializeField] private Color profitColor;
	private PercentageItemFactory itemFactory;

	/// <summary>
	/// The overlay sprite on top of the slider
	/// </summary>
	public Sprite Sprite
	{
		set => overlayImage.sprite = value;
	}

	private void OnDisable()
	{
		overlayImage.gameObject.SetActive(false);
		overlayObject.SetActive(false);
		if (profitText) profitText.gameObject.SetActive(false);
	}

	private void OnEnable()
	{
		overlayImage.gameObject.SetActive(true);
		overlayObject.SetActive(true);
		if (profitText) profitText.gameObject.SetActive(true);
	}

	private float Value => Mathf.Clamp01(MaxItems == 0 ? 0f : (float)itemCount / MaxItems);
	private int itemCount;
	/// <summary>
	/// The amount of items in the box
	/// </summary>
	public int ItemCount
	{
		get => itemCount;
		set
		{
			itemCount = value;
			UpdateComponents();
		}
	}

	private float ProfitMargin01 => Mathf.Clamp01(MaxItems == 0 ? 0f : (float)ProfitMargin / MaxItems);
	[SerializeField] private int profitMargin;
	/// <summary>
	/// How many items should be in the box to make it profitable
	/// </summary>
	public int ProfitMargin
	{
		get => profitMargin;
		set
		{
			profitMargin = value;
			UpdateComponents();
		}
	}

	[SerializeField] private int maxItems;
	/// <summary>
	/// How many items fit in the box at most
	/// </summary>
	public int MaxItems
	{
		get => maxItems;
		set
		{
			maxItems = value;
			UpdateComponents();
		}
	}

	private void Start()
	{
		itemFactory = FindObjectOfType<PercentageItemFactory>();
		InitializeComponents();
		UpdateComponents();
	}

	[ContextMenu("Initialize")]
	private void InitializeComponents()
	{
		if (!sliderSizeReference) return;
		float width = sliderSizeReference.rect.width;
		float height = sliderSizeReference.rect.height;
		if (profitBackground)
		{
			profitBackground.pivot = new Vector2(0f, 0.5f);
			profitBackground.anchorMin = new Vector2(0f, 0.5f);
			profitBackground.anchorMax = new Vector2(0f, 0.5f);
			profitBackground.localPosition = new Vector3(-width / 2f, 0f, 0f);
			profitBackground.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
			profitBackground.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
		}

		if (noProfitBackground)
		{
			noProfitBackground.pivot = new Vector2(0f, 0.5f);
			noProfitBackground.anchorMin = new Vector2(0f, 0.5f);
			noProfitBackground.anchorMax = new Vector2(0f, 0.5f);
			noProfitBackground.localPosition = new Vector3(-width / 2f, 0f, 0f);
			noProfitBackground.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width * ProfitMargin01);
			noProfitBackground.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
		}

		if (profitForeground)
		{
			profitForeground.pivot = new Vector2(0f, 0.5f);
			profitForeground.anchorMin = new Vector2(0f, 0.5f);
			profitForeground.anchorMax = new Vector2(0f, 0.5f);
			profitForeground.localPosition = new Vector3(-width / 2f, 0f, 0f);
			profitForeground.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
				Value * width);
			profitForeground.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
		}

		if (noProfitForeground)
		{
			noProfitForeground.pivot = new Vector2(0f, 0.5f);
			noProfitForeground.anchorMin = new Vector2(0f, 0.5f);
			noProfitForeground.anchorMax = new Vector2(0f, 0.5f);
			noProfitForeground.localPosition = new Vector3(-width / 2f, 0f, 0f);
			noProfitForeground.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
				Mathf.Min(Value, ProfitMargin01) * Value);
			noProfitForeground.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
		}
	}

	private void UpdateComponents()
	{
		if (!sliderSizeReference) return;
		float width = sliderSizeReference.rect.width;

		if (noProfitBackground)
		{
			noProfitBackground.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width * ProfitMargin01);
		}

		if (profitForeground)
		{
			profitForeground.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
				Value * width);
		}

		if (noProfitForeground)
		{
			noProfitForeground.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
				Mathf.Min(Value, ProfitMargin01) * width);
		}

		UpdateText();
	}

	private void UpdateText()
	{
		if (!profitText) return;
		var profit = (ItemCount - ProfitMargin) * (itemFactory ? itemFactory.ItemPrice : 10);
		profitText.color = profit < 0 ? lossColor : profitColor;
		profitText.text = profit.ToString("+0$;-#$");
	}
}