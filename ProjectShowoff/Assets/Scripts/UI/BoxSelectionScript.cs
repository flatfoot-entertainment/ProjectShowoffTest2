using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BoxSelectionScript : MonoBehaviour
{
	[SerializeField] private Color validPreview;
	[SerializeField] private Color invalidPreview;

	[System.Serializable]
	public class BoxSize
	{
		public GameObject prefab;
		public GameObject preview;
		public int price;
		public Button button;
		public int capacity;
		public int profitMargin;
		public Sprite progressBarSprite;
	}

	public bool BoxIsPresent => boxIsPresent;

	public int MinimumMoneyNeeded => BoxSettingAtIndex(0).Price;

	[SerializeField] private BoxSize[] boxSettings;
	private int boxSelectionIndex;

	[SerializeField] private Material previewMaterial;
	private FulfillmentCenter fulfillmentCenter;

	[SerializeField] private List<Button> selectionButtons = new List<Button>();

	[SerializeField] private TextMeshProUGUI boxCostText;
	[SerializeField] private Color PreviewColor => CanSpawnBox ? validPreview : invalidPreview;

	private bool CanBuyBox =>
		!boxIsPresent &&
		CurrentBox() != null &&
		GameHandler.Instance.Money >= CurrentBoxSetting().Price;


	[SerializeField, PlayModeReadOnly] private int startingMaxBoxesAllowedToBuy = 0;
	private int maxBoxAllowedToBuy = 0;
	private bool lastRequestedSelectionButtonState;

	private bool boxIsPresent, hasUnclosedBox;

	/// <summary>
	/// Did the player already buy a box or are they currently box-less?
	/// </summary>
	public bool PlayerHasNoBackupBox => !(boxIsPresent || hasUnclosedBox);

	private bool CanSpawnBox => CanBuyBox && spaceIsFree;
	private bool spaceIsFree;
	private int MaxBoxAllowedToBuy
	{
		get => maxBoxAllowedToBuy;
		set
		{
			maxBoxAllowedToBuy = value;
			UpdateConfirmButtons();
		}
	}

	private void Start()
	{
		// To update the cached value
		SetSelectionButtonsInteractable(true);
		MaxBoxAllowedToBuy = startingMaxBoxesAllowedToBuy;
		boxIsPresent = false;
		foreach (BoxSize box in boxSettings)
		{
			foreach (var r in box.preview.GetComponentsInChildren<Renderer>())
			{
				r.material = previewMaterial;
			}
			box.button.gameObject.SetActive(false);
		}

		fulfillmentCenter = FindObjectOfType<FulfillmentCenter>();
		boxSelectionIndex = 0;

		CurrentBox().preview.SetActive(true);
		CurrentBox().button.gameObject.SetActive(true);

		EventScript.Handler.Subscribe(EventType.BoxConveyorPlace, _ =>
		{
			if (hasUnclosedBox) return;
			boxIsPresent = false;
			CurrentBox().preview.SetActive(true);
			SetSelectionButtonsInteractable(true);
			SetConfirmButtonsInteractable(CanSpawnBox);
			previewMaterial.color = PreviewColor;
		});

		EventScript.Handler.Subscribe(EventType.ManageMoney, _ => OnMoneyChange());

		EventScript.Handler.Subscribe(EventType.BoxUpgrade, _ => MaxBoxAllowedToBuy++);

		EventScript.Handler.Subscribe(EventType.BoxClose, _ => hasUnclosedBox = false);

		OnMoneyChange();
		boxCostText.text = $"${CurrentBoxSetting().Price}";
	}

	public void ChangeBox(int direction)
	{
		int previousIndex = boxSelectionIndex;
		int lim = maxBoxAllowedToBuy + 1 < boxSettings.Length ? maxBoxAllowedToBuy + 1 : boxSettings.Length;
		boxSelectionIndex += direction;
		boxSelectionIndex = boxSelectionIndex < 0 ? lim - 1 : boxSelectionIndex;
		boxSelectionIndex = boxSelectionIndex >= lim ? 0 : boxSelectionIndex;

		if (previousIndex != boxSelectionIndex)
		{
			boxSettings[previousIndex].preview.SetActive(false);
			boxSettings[previousIndex].button.gameObject.SetActive(false);
		}

		boxSettings[boxSelectionIndex].preview.SetActive(true);
		boxSettings[boxSelectionIndex].button.gameObject.SetActive(true);
		OnMoneyChange();
		boxCostText.text = $"${CurrentBoxSetting().Price}";
	}

	private void OnMoneyChange()
	{
		SetConfirmButtonsInteractable(CanSpawnBox);
		previewMaterial.color = PreviewColor;
	}

	private void Update()
	{
		if (!CanBuyBox) return;

		var saved = fulfillmentCenter.SpaceIsFree();
		if (saved == spaceIsFree) return;
		spaceIsFree = saved;

		SetSelectionButtonsInteractable(true);
		SetConfirmButtonsInteractable(CanSpawnBox);

		previewMaterial.color = PreviewColor;
	}

	public void ConfirmBox()
	{
		if (!CanSpawnBox) return;

		CurrentBox().preview.SetActive(false);

		fulfillmentCenter.SpawnBox(CurrentBox().prefab, CurrentBoxSetting().Capacity,
			CurrentBoxSetting().ProfitMargin, CurrentBox().progressBarSprite);

		EventScript.Handler.BroadcastEvent(new ManageMoneyEvent(-CurrentBoxSetting().Price));
		SetConfirmButtonsInteractable(false);
		SetSelectionButtonsInteractable(false);
		boxIsPresent = true;
		hasUnclosedBox = true;
	}

	private void SetSelectionButtonsInteractable(bool val)
	{
		lastRequestedSelectionButtonState = val;
		foreach (Button button in selectionButtons) button.interactable = maxBoxAllowedToBuy > 0 && val;
	}

	private BoxSize CurrentBox()
	{
		return boxSettings[boxSelectionIndex];
	}

	private void SetConfirmButtonsInteractable(bool val)
	{
		foreach (var setting in boxSettings) setting.button.interactable = val;
	}

	private void UpdateConfirmButtons()
	{
		foreach (Button button in selectionButtons)
			button.interactable = MaxBoxAllowedToBuy > 0 && lastRequestedSelectionButtonState;
	}

	private Config.BoxSetting CurrentBoxSetting()
	{
		// This could be cached, but it's rarely used so the performance gain would be marginal
		return BoxSettingAtIndex(boxSelectionIndex);
	}

	private Config.BoxSetting BoxSettingAtIndex(int index)
	{
		var settings = ConfigLoader.GameConfig?.BoxSettings;
		return settings == null || settings.Count <= index
			? new Config.BoxSetting
			{
				Price = CurrentBox().price,
				Capacity = CurrentBox().capacity,
				ProfitMargin = CurrentBox().profitMargin
			}
			: ConfigLoader.GameConfig.BoxSettings[index];
	}
}
