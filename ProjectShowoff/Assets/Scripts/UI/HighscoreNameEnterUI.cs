﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreNameEnterUI : MonoBehaviour
{
	[SerializeField] private TMP_InputField inputField;
	[SerializeField] private Button submit;
	[SerializeField, PlayModeReadOnly] private int maxChars;

	public bool HasName => !string.IsNullOrWhiteSpace(inputField.text);
	public string Name => inputField.text;

	public event System.Action OnSubmit;

	private void Start()
	{
		submit.interactable = HasName;
		inputField.characterValidation = TMP_InputField.CharacterValidation.Alphanumeric;
		inputField.characterLimit = maxChars;
		inputField.onValueChanged.AddListener(_ => submit.interactable = HasName);
		submit.onClick.AddListener(() =>
		{
			if (HasName) OnSubmit?.Invoke();
		});
	}
}
