using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EndGameScript : MonoBehaviour
{
	[SerializeField] private HighscoreNameEnterUI ui;
	[SerializeField] private GameObject actualDisplay;
	[SerializeField] private ChangeGameState gameStateScript;
	[SerializeField] private TextMeshProUGUI timeSpentText;
	[SerializeField] private int numHighscores;
	[SerializeField] private TextMeshProUGUI highscoreText;
	[SerializeField] private string formatString;

	private void OnEnable()
	{
		Time.timeScale = 0f;
		if (!ui || !Highscores.TimeIsHighscore(gameStateScript.TimeSpent))
		{
			if (ui) ui.gameObject.SetActive(false);
			actualDisplay.SetActive(true);
			ShowActualUI();
		}
		else
		{
			ui.gameObject.SetActive(true);
			actualDisplay.SetActive(false);
			ui.OnSubmit += ShowActualUI;
		}
	}

	private void ShowActualUI()
	{
		if (!GameHandler.Instance.hasLost)
			Highscores.AddEntry(ui.Name, gameStateScript.TimeSpent);
		if (ui) ui.gameObject.SetActive(false);
		actualDisplay.SetActive(true);

		timeSpentText.text = string.Format(formatString, gameStateScript.TimeSpent);

		if (highscoreText)
		{
			string formatted = "";
			for (var i = 0; i < numHighscores && i < Highscores.Get.Count; i++)
			{
				formatted += $"{i + 1}. {Highscores.Get[i].name}: {Highscores.Get[i].time}s\n";
			}

			formatted = formatted.TrimEnd('\n');
			highscoreText.text = formatted;
		}
	}
}
