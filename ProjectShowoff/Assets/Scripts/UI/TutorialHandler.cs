﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using DG.Tweening;
using UnityEngine.Events;

public class TutorialHandler : MonoBehaviour
{
	[SerializeField] private VideoPlayer tutorialPlayer;
	[SerializeField] private RawImage videoTarget;
	[SerializeField] private RenderTexture renderTexture;

	private bool playing;
	private bool isFirstTime;

	[Header("Animation")]
	
	[SerializeField] private Ease fade;
	[SerializeField] private float fadingTime;

	[SerializeField] private List<GameObject> enableInPackaging;
	[SerializeField] private List<GameObject> enableInPlanet;
	private CameraMoveEvent.CameraState currentState = CameraMoveEvent.CameraState.Shipping;
	
	[Header("Callbacks")]
	
	[SerializeField] private UnityEvent onFirstCompletion;
	[SerializeField] private UnityEvent onNonFirstCompletion;

	private Sequence seq;

	private void Awake()
	{
		ResetRenderTexture();
		tutorialPlayer.loopPointReached += _ => OnComplete();
		tutorialPlayer.Prepare();
		tutorialPlayer.Pause();
		videoTarget.gameObject.SetActive(false);
		videoTarget.color = new Color(1, 1, 1, 0);
	}

	private void Start()
	{
		EventScript.Handler.Subscribe(EventType.CameraMove, e => currentState = ((CameraMoveEvent)e).NewState);
	}

	private void ResetRenderTexture()
	{
		RenderTexture rt = RenderTexture.active;
		RenderTexture.active = renderTexture;
		GL.Clear(true, true, Color.clear);
		RenderTexture.active = rt;
	}

	public void ShowTutorial(bool firstTime)
	{
		if (playing) return;
		playing = true;
		isFirstTime = firstTime;
		Time.timeScale = 0;
		videoTarget.gameObject.SetActive(true);
		tutorialPlayer.Play();
		seq = DOTween.Sequence();
		seq.Append(videoTarget.DOColor(Color.white, fadingTime).SetEase(fade).SetUpdate(true).SetOptions(true));
		seq.AppendInterval((float) tutorialPlayer.length - 2 * fadingTime);
		seq.Append(videoTarget.DOColor(new Color(1, 1, 1, 0), fadingTime).SetEase(fade).SetUpdate(true)
			.SetOptions(true));
		seq.SetUpdate(true);
	}

	public void SkipTutorial()
	{
		seq.Kill();
		// The sequence might still be running
		videoTarget.DOKill();
		var timeToFade = Mathf.Min(fadingTime, (float) (tutorialPlayer.length - tutorialPlayer.time));
		seq = DOTween.Sequence();
		seq.Append(videoTarget.DOColor(new Color(1, 1, 1, 0), timeToFade).SetEase(fade).SetUpdate(true)
			.SetOptions(true));
		seq.AppendCallback(Stop);
		seq.SetUpdate(true);
	}

	private void Stop()
	{
		playing = false;
		tutorialPlayer.Stop();
		videoTarget.gameObject.SetActive(false);
		ResetRenderTexture();
		switch (currentState)
		{
			case CameraMoveEvent.CameraState.Packaging:
			{
				foreach (var o in enableInPackaging)
					if (o)
						o.SetActive(true);
				break;
			}
			case CameraMoveEvent.CameraState.Shipping:
			{
				foreach (var o in enableInPlanet)
					if (o)
						o.SetActive(true);
				break;
			}
		}
		if (isFirstTime) onFirstCompletion?.Invoke();
		else onNonFirstCompletion?.Invoke();
	}

	private void OnComplete()
	{
		Stop();
	}
}
