using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlanetRequirementsUI : MonoBehaviour
{
	[SerializeField] private List<PlanetRequirementUI> requirements;
	[SerializeField] private Sprite foodIcon;
	[SerializeField] private Sprite fuelIcon;
	[SerializeField] private Sprite mechIcon;
	[SerializeField] private Sprite medIcon;

	private Sprite MatchIcon(ItemType type)
	{
		return type switch
		{
			ItemType.Food => foodIcon,
			ItemType.MechanicalParts => mechIcon,
			ItemType.Medicine => medIcon,
			ItemType.Fuel => fuelIcon,
			_ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
		};
	}

	public void UpdateContents(Dictionary<ItemType, int> newContents)
	{
		foreach (var requirement in requirements) requirement.gameObject.SetActive(false);
		int index = 0;
		foreach (var type in Extensions.GetValues<ItemType>()) 
		{
			if (newContents.ContainsKey(type))
			{
				if (index < requirements.Count)
				{
					requirements[index].gameObject.SetActive(true);
					requirements[index].UpdateContents(MatchIcon(type), newContents[type]);
				}
				index++;
			}
		}
	}
}
