using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveByEvent : MonoBehaviour
{

	//do we need this?
	[SerializeField] private GameObject confirmationPanel;
	// Start is called before the first frame update
	void Start()
	{
		EventScript.Handler.Subscribe(EventType.ManageConfirmationPanel, _ => EnableConfirmationPanel());
	}

	private void EnableConfirmationPanel()
	{
		confirmationPanel.SetActive(true);
	}
}
