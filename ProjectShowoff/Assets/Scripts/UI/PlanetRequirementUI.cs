using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlanetRequirementUI : MonoBehaviour
{
	private TextMeshProUGUI Number
	{
		get
		{
			if (!number) number = GetComponentInChildren<TextMeshProUGUI>();
			return number;
		}
	}
	private TextMeshProUGUI number;

	private Image Icon
	{
		get
		{
			if (!icon) icon = GetComponentInChildren<Image>();
			return icon;
		}
	}
	private Image icon;

	private void Start()
	{
		number = GetComponentInChildren<TextMeshProUGUI>();
		if (!number) Debug.LogError("I want to commit die");
		icon = GetComponentInChildren<Image>();
		if (!icon) Debug.LogError("I don't want to do this anymore");
	}

	public void UpdateContents(Sprite image, int num)
	{
		Number.text = $"{num}";
		Icon.sprite = image;
	}
}
