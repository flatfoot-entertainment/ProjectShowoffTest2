using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;


//This script shows/hides the amount of money won/lost when the ManageMoney event is called 
public class MoneyEffectScript : MonoBehaviour
{
	[SerializeField] private Color profitColor, lossColor;
	[SerializeField] private float timeToShow = 5f;
	private TextMeshProUGUI moneyEffectText;

	private void Start()
	{
		//i think it should subscribe to things...gotta look them up
		moneyEffectText = GetComponent<TextMeshProUGUI>();
		moneyEffectText.text = "";
		EventScript.Handler.Subscribe(EventType.ManageMoney, ManageMoneyUsed);
	}

	private void ManageMoneyUsed(Event e)
	{
		if (!(e is ManageMoneyEvent moneyEvent)) return;
		if (moneyEvent.Amount == 0) return;
		transform.localScale = Vector3.one;
		if (moneyEvent.Amount < 0)
		{
			moneyEffectText.color = lossColor;
			moneyEffectText.text = moneyEvent.Amount.ToString();
		}
		else if (moneyEvent.Amount > 0)
		{
			moneyEffectText.color = profitColor;
			moneyEffectText.text = $"+{moneyEvent.Amount}";
		}

		transform.DOKill();
		transform.DOScale(Vector3.zero, timeToShow);
	}
}
