using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmationPanelScript : MonoBehaviour
{
	[SerializeField] private UpgradeType upgradeReceived;

	[SerializeField] private Button yesButton;
	private void Awake()
	{
		EventScript.Handler.Subscribe(EventType.ManageConfirmationPanel, OnUpgradeReceived);
	}

	public void CancelUpgrade()
	{
		EventScript.Handler.BroadcastEvent(new ContinueAfterPromptEvent(false, upgradeReceived));
		gameObject.SetActive(false);
	}

	public void ContinueAfterUpgradePrompt()
	{
		if (yesButton.interactable)
		{
			EventScript.Handler.BroadcastEvent(new ContinueAfterPromptEvent(true, upgradeReceived));
			gameObject.SetActive(false);
		}
	}

	private void OnUpgradeReceived(Event e)
	{
		ManageConfirmationPanelEvent confirmEvent = e as ManageConfirmationPanelEvent;
		upgradeReceived = confirmEvent.UpgradeType;
		yesButton.interactable = !confirmEvent.CanPurchase;
		Debug.Log($"the yes button is interactable:{yesButton.interactable}");
	}
}
