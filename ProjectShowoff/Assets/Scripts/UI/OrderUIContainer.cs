using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;

public class OrderUIContainer : MonoBehaviour
{
	[SerializeField] private Planet planet;
	[SerializeField] private Image planetImage;

	[SerializeField] private Image timerImage;

	[SerializeField] private Transform adaptiveField;
	[SerializeField] private GameObject requirementContainerPrefab;

	[SerializeField] private int startingTime;
	[SerializeField] private float timeLeft;
	[SerializeField] private float timeLeftClamped;
	[SerializeField] private float blinkSpeed = 5;
	private Dictionary<ItemType, RequirementUIContainer> requirementUIContainers =
		new Dictionary<ItemType, RequirementUIContainer>();


	private void OnEnable()
	{
		// Set it to be the last child on enable
		transform.SetSiblingIndex(transform.parent.childCount - 1);
		Image img = timerImage.GetComponent<Image>();
		Color c = img.color;
		c.a = 1f;
		img.color = c;
	}

	private void Update()
	{
		UpdateTimer();
	}

	private void UpdateTimer()
	{
		timeLeft -= Time.deltaTime;
		timeLeftClamped = Mathf.InverseLerp(0, startingTime, timeLeft);
		timerImage.fillAmount = timeLeftClamped;
		if (timeLeft <= startingTime / 5f)
		{
			StartTimerBlinking();
		}
		if (timeLeft <= 0f)
		{
			EventScript.Handler.BroadcastEvent(new ManageMoneyEvent((int)-planet.MoneyLostFromOrder));
			EventScript.Handler.BroadcastEvent(new ManageMoneyLeftEvent());
			gameObject.SetActive(false);
		}
	}

	public void SetupContainer(Planet pPlanet)
	{
		planet = pPlanet;
		if (planetImage != null) planetImage.sprite = planet.PlanetImage;
		foreach (var type in Extensions.GetValues<ItemType>())
		{
			GameObject requirementContainer = Instantiate(requirementContainerPrefab, Vector3.zero, Quaternion.identity,
				adaptiveField);
			RequirementUIContainer requirementUIScript = requirementContainer.GetComponent<RequirementUIContainer>();
			requirementUIScript.SetupRequirementContainer(type, 0);
			requirementUIContainers.Add(type, requirementUIScript);
			requirementContainer.SetActive(false);
		}
		foreach (ItemType type in planet.needs.Keys)
		{
			requirementUIContainers[type].gameObject.SetActive(true);
			requirementUIContainers[type].UpdateContainer(planet.needs[type]);
		}
	}
	public void NewOrder(Dictionary<ItemType, int> needsLeft, int time)
	{
		startingTime = time;
		timeLeft = startingTime;
		UpdateRequirementUIContainers(needsLeft);
	}

	public void UpdateRequirementUIContainers(Dictionary<ItemType, int> needsLeft)
	{
		foreach (ItemType type in requirementUIContainers.Keys)
		{
			if (needsLeft.ContainsKey(type))
			{
				requirementUIContainers[type].gameObject.SetActive(true);
				requirementUIContainers[type].UpdateContainer(needsLeft[type]);
			}
			else
			{
				requirementUIContainers[type].DisableContainer();
			}
		}

		if (needsLeft.Count <= 0) gameObject.SetActive(false);
	}

	private void StartTimerBlinking()
	{
		Color c = timerImage.color;
		c.a = Mathf.Abs(Mathf.Sin(Time.time * blinkSpeed));
		timerImage.color = c;
	}

}
