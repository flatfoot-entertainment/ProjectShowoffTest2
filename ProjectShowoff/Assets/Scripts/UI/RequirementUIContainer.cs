using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class RequirementUIContainer : MonoBehaviour
{
	[SerializeField] private Sprite foodSprite, mechanicalPartsSprite, fuelSprite, medicineSprite;
	[SerializeField] private Image requirementImage;
	[SerializeField] private TextMeshProUGUI amountText;

	[SerializeField] private ItemType type;
	[SerializeField] private int amount;

	public void SetupRequirementContainer(ItemType itemType, int itemAmount){
		gameObject.SetActive(true);
		Sprite requirementSprite = null;
		switch(itemType){
			case ItemType.Food:
			requirementSprite = foodSprite;
			break;
			case ItemType.MechanicalParts:
			requirementSprite = mechanicalPartsSprite;
			break;
			case ItemType.Fuel:
			requirementSprite = fuelSprite;
			break;
			case ItemType.Medicine:
			requirementSprite = medicineSprite;
			break;
		}
		type = itemType;
		amount = itemAmount;
		if(requirementSprite != null){
			requirementImage.sprite = requirementSprite;
		}
		amountText.text = itemAmount.ToString();
	}

	public void UpdateContainer(int updatedAmount){
		gameObject.SetActive(true);
		amount = updatedAmount;
		amountText.text = amount.ToString();
	}

	public void DisableContainer(){
		gameObject.SetActive(false);
	}
}
