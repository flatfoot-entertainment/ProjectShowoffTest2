﻿using UnityEngine;

/// <summary>
/// Used to draw a Layer picker
/// </summary>
public class LayerAttribute : PropertyAttribute
{ }
