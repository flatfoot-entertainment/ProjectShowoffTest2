﻿using UnityEngine;

/// <summary>
/// Use this to draw a min max slider
/// </summary>
public class MinMaxSliderAttribute : PropertyAttribute
{
	public float Min { get; }
	public float Max { get; }

	public MinMaxSliderAttribute(float min, float max)
	{
		Min = min;
		Max = max;
	}
}
