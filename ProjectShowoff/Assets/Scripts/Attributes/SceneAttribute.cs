﻿using UnityEngine;

/// <summary>
/// Use this to be able to pick a scene asset
/// </summary>
public class SceneAttribute : PropertyAttribute
{ }