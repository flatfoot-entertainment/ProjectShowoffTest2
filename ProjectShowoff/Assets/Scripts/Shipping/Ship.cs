using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using DG.Tweening;

public class Ship : MonoBehaviour
{
	public event System.Action OnArrival;
	/// <summary>
	/// The box inside the ship
	/// </summary>
	public Container box { get; set; }
	private Planet target;
	private Vector3 startPos;
	private Vector3 startRotation;
	private Vector3 startScale;
	private Sequence sequence;

	[Header("\"Animation\" settings")]
	[SerializeField] private float travelTime;
	[SerializeField] private Ease travelEase;
	[SerializeField] private float turnTime;
	[SerializeField] private Ease turnEase;
	[SerializeField] private float scaleTime;
	[SerializeField] private Ease scaleOutEase;
	[SerializeField] private Ease scaleInEase;
	[SerializeField] private float scaleFactor;

	private bool delivered;

	private void Start()
	{
		startPos = transform.position;
		startRotation = transform.rotation.eulerAngles;
		startScale = transform.localScale;

	}

	private void OnDrawGizmos()
	{
		if (target != null)
		{
			Gizmos.DrawLine(transform.position, target.transform.position);
		}
	}

	/// <summary>
	/// Send the ship to deliver the contained box to the specified planet
	/// </summary>
	public void DeliverTo(Planet planet)
	{
		target = planet;
		sequence = DOTween.Sequence();
		sequence.Append(transform.DOLookAt(target.transform.position, turnTime).SetEase(turnEase));
		sequence.Insert(0f, transform.DOMove(target.transform.position, travelTime).SetEase(travelEase));
		sequence.Insert(travelTime - scaleTime, transform.DOScale(startScale * scaleFactor, scaleTime).SetEase(scaleOutEase));
	}

	private void OnTriggerEnter(Collider other)
	{
		if (delivered || !target || other.gameObject != target.gameObject) return;
		delivered = true;
		// Ship has been delivered
		target.Deliver(box);
		box = null;
		if (sequence.IsActive()) sequence.Kill();
		StartCoroutine(Return());
	}

	private IEnumerator Return()
	{
		sequence = DOTween.Sequence();
		sequence.Append(transform.DOLookAt(startPos, turnTime).SetEase(turnEase));
		sequence.Append(transform.DOMove(startPos, travelTime).SetEase(travelEase));
		sequence.Insert(turnTime, transform.DOScale(startScale, scaleTime).SetEase(scaleInEase));
		sequence.Insert(travelTime, transform.DORotate(startRotation, turnTime).SetEase(turnEase));
		yield return new WaitWhile(() => sequence.IsActive() && sequence.IsPlaying());
		OnArrival?.Invoke();
		EventScript.Handler.BroadcastEvent(new ManageButtonBlinkingEvent(EventType.ReturnToPackagingButtonBlink, true));
	}

	/// <summary>
	/// Reset this ship so it can be safely used again
	/// </summary>
	public void ResetShip()
	{
		delivered = false;
		sequence = null;
		target = null;
	}
}
