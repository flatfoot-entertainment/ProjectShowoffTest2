using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShipComponents : MonoBehaviour
{
	/// <summary>
	/// Called when any contents changed
	/// </summary>
	public event System.Action OnAnyContentsChanged;
	/// <summary>
	/// Called when the ship returns
	/// </summary>
	public event System.Action OnArrival;
	
	[SerializeField] private Ship ship;
	[SerializeField] private List<ShipLoadingHandler> shipLoadingHandlers;
	private Ship Ship => ship;
	private IEnumerable<ShipLoadingHandler> LoadingHandlers => shipLoadingHandlers;

	/// <summary>
	/// The items in this object
	/// </summary>
	public Container StoredItems { get; private set; } = new Container();

	public bool CanShip => StoredItems.Contents.Count > 0 && !currentlyShipping;

	private bool currentlyShipping = false;

	private void Start()
	{
		Ship.OnArrival += () =>
		{
			currentlyShipping = false;
			OnArrival?.Invoke();
		};
		foreach (var handler in LoadingHandlers)
		{
			handler.OnContentsChanged += () => OnHandlerContentsChanged(handler);
		}
	}

	/// <summary>
	/// Send the ship to the specified planet
	/// </summary>
	public void SendShip(Planet to)
	{
		if (currentlyShipping) return;

		// Reset the ship and load the box into it
		ship.ResetShip();
		ship.box = StoredItems;
		
		// Tell the ship to be yeeted
		ship.DeliverTo(to);
		// Reset StoredItems
		StoredItems = new Container();
		// Contents have changed, so call the callback
		OnAnyContentsChanged?.Invoke();

		currentlyShipping = true;
	}

	private void OnHandlerContentsChanged(ShipLoadingHandler handler)
	{
		StoredItems += handler.Box;
		handler.ResetLoadingHandler();
		OnAnyContentsChanged?.Invoke();
	}
}
