using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class FulfillmentCenter : MonoBehaviour
{
	[SerializeField] private Transform boxPos;
	[SerializeField] private BoxContentsUI boxContentsUI;

	private ItemBoxController fillableBox;

	[Header("Obstructed space")]

	[SerializeField] private Vector3 center;
	[SerializeField] private Vector3 halfExtends;
	[SerializeField] private Vector3 orientation;
	[SerializeField] private LayerMask mask;
	[SerializeField] private BoxFillSlider slider;

#if UNITY_EDITOR
	private void OnDrawGizmos()
	{
		var oldMat = Gizmos.matrix;
		Matrix4x4 rot = Matrix4x4.Translate(boxPos.position + center) * Matrix4x4.Rotate(Quaternion.Euler(orientation));
		Gizmos.matrix = rot;

		Gizmos.DrawWireCube(Vector3.zero, halfExtends * 2);

		Gizmos.matrix = oldMat;
	}
#endif

	private void Start()
	{
		slider.gameObject.SetActive(false);
	}

	/// <summary>
	/// Can the current box be shipped?
	/// </summary>
	public bool CanShipBox()
	{
		return fillableBox && fillableBox.Shippable;
	}

	/// <summary>
	/// Close the current box
	/// </summary>
	public void CloseBox()
	{
		slider.gameObject.SetActive(false);
		fillableBox.Close();
		boxContentsUI.ClearUI();
		//EventScript.Handler.BroadcastEvent(new ShipButtonBlinkEvent(true));
		EventScript.Handler.BroadcastEvent(new BoxCloseEvent());
	}

	/// <summary>
	/// Spawn a new box, with the specified settings
	/// </summary>
	public void SpawnBox(GameObject boxPrefab, int capacity, int profitMargin, Sprite progressSprite)
	{
		if (fillableBox) return;
		slider.MaxItems = capacity;
		slider.ItemCount = 0;
		slider.ProfitMargin = profitMargin;
		slider.Sprite = progressSprite;
		slider.gameObject.SetActive(true);
		fillableBox = Instantiate(boxPrefab, boxPos.position, Quaternion.identity).GetComponent<ItemBoxController>();
		fillableBox.OnContentsUpdated += () =>
		{
			boxContentsUI.UpdateContents(fillableBox.Box.BoxContents);
			slider.MaxItems = capacity;
			slider.ItemCount = fillableBox.Box.Contents.Count;
		};
	}

	/// <summary>
	/// Check if nothing is obstructing the place around the box
	/// </summary>
	public bool SpaceIsFree()
	{
		var coll = new Collider[1];
		return Physics.OverlapBoxNonAlloc(
			boxPos.position + center,
			halfExtends,
			coll,
			Quaternion.Euler(orientation), mask
		) == 0;
	}
}
