using System.Collections;
using System.Collections.Generic;
// using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;

public class Planet : MonoBehaviour
{
	public CanvasScaler Scaler
	{
		get => scaler;
		set => scaler = value;
	}

	public PlanetaryShipmentCenter PlanetaryShipmentCenter
	{
		get => planetaryShipmentCenter;
		set => planetaryShipmentCenter = value;
	}

	public Sprite PlanetImage
	{
		get => planetImage;
		set => planetImage = value;
	}

	public int MoneyLostFromOrder => moneyLostFromOrder;
	[SerializeField] private Outline.Mode outlineMode;
	[SerializeField] private Color outlineColor;
	[SerializeField] private float outlineWidth;

	[SerializeField] private Sprite planetImage;
	[SerializeField] private CanvasScaler scaler;

	[SerializeField] private OrderUIContainer orderUIContainer;
	[SerializeField] private PlanetaryShipmentCenter planetaryShipmentCenter;

	[SerializeField] private PercentageItemFactory itemFactory;

	[SerializeField] private int moneyLostFromOrder;

	[SerializeField] private PlanetRequirementsUI requirementsUI;

	private Coroutine orderCoroutine;

	private Vector2Int amountOfItemsNeededPerOrder = new Vector2Int(1, 3);

	public Dictionary<ItemType, int> needs { get; } = new Dictionary<ItemType, int>();
	public bool HasNeeds => needs.Count > 0;

	public bool HasOrder { get; private set; }

	private Outline outline;

	private void Start()
	{
		orderUIContainer.SetupContainer(this);
		// Clear the order thing
		requirementsUI.UpdateContents(new Dictionary<ItemType, int>());
	}

	public void Deliver(Container box)
	{
		if (needs.Count <= 0)
		{
			EventScript.Handler.BroadcastEvent(new ManageMoneyLeftEvent());
			return;
		}
		int money = 0;
		foreach (ItemBox b in box.Contents)
		{
			foreach (Item i in b.Contents)
			{
				if (needs.ContainsKey(i.Type))
				{
					needs[i.Type] -= 1;
					if (needs[i.Type] <= 0)
					{
						needs.Remove(i.Type);
					}
					// Only add money if the planet actually needed it
					money += i.Price;
				}
			}
		}
		// Add the money to the player

		if (needs.Count <= 0)
		{
			EventScript.Handler.BroadcastEvent(new OrderCompleteEvent());
			HasOrder = false;
			orderUIContainer.gameObject.SetActive(false);
			needs.Clear();
			if (orderCoroutine != null) StopCoroutine(orderCoroutine);
		}

		EventScript.Handler.BroadcastEvent(new ManageMoneyEvent(money));
		EventScript.Handler.BroadcastEvent(new ManageMoneyLeftEvent());
		orderUIContainer.UpdateRequirementUIContainers(needs);
		requirementsUI.UpdateContents(needs);
	}

	public void Deselect()
	{
		Destroy(outline);
	}

	public void Select()
	{
		outline = gameObject.AddComponent<Outline>();
		outline.OutlineColor = outlineColor;
		outline.OutlineMode = outlineMode;
		outline.OutlineWidth = outlineWidth;
	}

	private void OnMouseDown()
	{
		planetaryShipmentCenter.OnPlanetClicked(this);
	}

	private IEnumerator SingleOrder(int orderLength)
	{
		orderUIContainer.NewOrder(needs, orderLength);
		orderUIContainer.gameObject.SetActive(true);
		
		yield return new WaitForSeconds(orderLength);
		
		if (!HasOrder) yield break; // To make sure the order doesn't get completed twice

		// At this point the order is failed
		orderUIContainer.gameObject.SetActive(false);
		needs.Clear();
		requirementsUI.UpdateContents(needs);
		HasOrder = false;
		orderCoroutine = null;
		// Has been stopped automatically
	}

	private void InitRandom()
	{
		needs.Clear();
		int numProps = amountOfItemsNeededPerOrder.Random();
		for (int i = 0; i < numProps; i++)
		{
			ItemType t = itemFactory.GetRandomType();
			if (needs.ContainsKey(t)) needs[t] += 1;
			else needs[t] = 1;
		}
	}

	// Tell this planet that it can state demands now. Returned will be the time of the order
	public void NewOrder(int orderTime, int pMoneyLostFromOrder)
	{
		moneyLostFromOrder = pMoneyLostFromOrder;
		InitRandom();
		orderUIContainer.UpdateRequirementUIContainers(needs);
		requirementsUI.UpdateContents(needs);
		HasOrder = true;
		orderCoroutine = StartCoroutine(SingleOrder(orderTime));
	}

	public void UpdateOrderSize(Vector2Int orderSize)
	{
		amountOfItemsNeededPerOrder = orderSize;
	}
}
