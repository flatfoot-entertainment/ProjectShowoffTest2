using UnityEngine;

public class ShipLoadingHandler : MonoBehaviour
{
	/// <summary>
	/// Called when something gets added to the loading handler
	/// </summary>
	public event System.Action OnContentsChanged;
	/// <summary>
	/// The stuff inside this loading handler
	/// </summary>
	public Container Box { get; private set; }

	void Start()
	{
		Box = new Container();
	}

	private void OnCollisionEnter(Collision other)
	{
		var comp = other.gameObject.GetComponent<ItemBoxScript>();
		if (comp)
		{
			Box.AddToBox(comp.contained);
			OnContentsChanged?.Invoke();
			// Is this a good idea? YES... well maybe, but it works, so...
			EventScript.Handler.BroadcastEvent(new BoxConveyorPlaceEvent());
			//EventScript.Handler.BroadcastEvent(new ShipShipButtonBlinkEvent(true));
			Destroy(other.gameObject);
		}
	}

	public void ResetLoadingHandler()
	{
		Box = new Container();
	}
}
