using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetaryShipmentCenter : MonoBehaviour
{
	private Planet selectedPlanet;
	[SerializeField] private ShipComponents ship;
	[SerializeField] private List<BoxContentsUI> shipContentsUIs;
	[SerializeField] private Button shipButton;

	private bool inShipment = false;

	private void Awake()
	{
		ship.OnArrival += OnShipReturned;
		ship.OnAnyContentsChanged += () =>
		{
			shipButton.interactable = ship.CanShip && selectedPlanet;
			foreach (var ui in shipContentsUIs)
			{
				ui.UpdateContents(ship.StoredItems.Items);
			}
		};
		shipButton.interactable = ship.CanShip && selectedPlanet;
	}

	/// <summary>
	/// Deliver the ship to the selected planet. If no planet is selected, nothing happens.
	/// </summary>
	public void DeliverSelected()
	{
		// We need a selected planet
		if (!selectedPlanet) return;
		ship.SendShip(selectedPlanet);
		shipButton.interactable = false;

		selectedPlanet.Deselect();

		selectedPlanet = null;

		inShipment = true;
	}

	public void OnPlanetClicked(Planet planet)
	{
		// Only allow selecting a planet, if you can actually ship stuff
		if (inShipment || !ship.CanShip) return;
		if (selectedPlanet) selectedPlanet.Deselect();
		selectedPlanet = planet;
		selectedPlanet.Select();
		shipButton.interactable = ship.CanShip && selectedPlanet;
	}

	private void OnShipReturned()
	{
		shipButton.interactable = ship.CanShip && selectedPlanet;
		inShipment = false;
	}
}
