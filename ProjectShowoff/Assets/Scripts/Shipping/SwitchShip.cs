using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchShip : MonoBehaviour
{
	[SerializeField] private List<Transform> shipModels;
	[SerializeField] private int whenIsSecondShipShown, whenThirdShipIsShown;

	private void Start()
	{
		shipModels = new List<Transform>();
		for (int i = 0; i < transform.childCount; i++)
		{
			shipModels.Add(transform.GetChild(i));
		}
		EventScript.Handler.Subscribe(EventType.SwitchShip, OnSwitchShip);
	}

	private void OnSwitchShip(Event e)
	{
		SwitchShipEvent switchEvent = e as SwitchShipEvent;
		if (switchEvent.Index >= whenIsSecondShipShown)
		{
			shipModels[0].gameObject.SetActive(false);
			shipModels[1].gameObject.SetActive(true);
		}
		if (switchEvent.Index >= whenThirdShipIsShown)
		{
			shipModels[1].gameObject.SetActive(false);
			shipModels[2].gameObject.SetActive(true);
		}
	}
}
