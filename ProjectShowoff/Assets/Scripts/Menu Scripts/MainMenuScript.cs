using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenuScript : MonoBehaviour
{
	private Button[] mainMenuButtons;
	private ChangeGameState changeGameState;

	[SerializeField] private GameObject triggers;
	
	private void Start()
	{
		mainMenuButtons = GetComponentsInChildren<Button>();
	}
	public void DisableMainMenuButtons()
	{
		foreach (Button b in mainMenuButtons)
		{
			b.interactable = false;
		}
	}
	public void QuitGame()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	/// <summary>
	/// Restart the game
	/// </summary>
	public void ResetGame()
	{
		Time.timeScale = 1f;
		triggers.SetActive(false);
		StartCoroutine(ResetCoroutine());
	}
	private IEnumerator ResetCoroutine()
	{
		yield return new WaitForSeconds(2f);
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
