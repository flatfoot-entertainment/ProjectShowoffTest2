using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;

public class CameraTranslate : MonoBehaviour
{
	[SerializeField] private CinemachineBrain brain;
	private CinemachineVirtualCamera lastCam;
	[SerializeField] private CinemachineVirtualCamera packagingVCam;
	[SerializeField] private CinemachineVirtualCamera shippingVCam;

	[SerializeField] private CinemachineVirtualCamera mainMenuCam;
	[SerializeField] private CameraMoveEvent.CameraState initialView;

	/// <summary>
	/// An enumerator enumerating over all cameras
	/// </summary>
	/// <returns></returns>
	private IEnumerable<CinemachineVirtualCamera> AllCams()
	{
		yield return packagingVCam;
		yield return shippingVCam;
		yield return mainMenuCam;
	}

	private void Start()
	{
		brain = Camera.main.GetComponent<CinemachineBrain>();

		EventScript.Handler.Subscribe(EventType.CameraMove, (e) =>
		{
			OnCamMove(((CameraMoveEvent)e).NewState);
		});

		foreach (CinemachineVirtualCamera cam in AllCams()) cam.Priority = 0;

		// execute stuff one frame after start
		Sequence seq = DOTween.Sequence();
		seq.AppendCallback(() =>
		{
			EventScript.Handler.BroadcastEvent(
				new CameraMoveEvent(initialView)
			);
		});
		OnCamMove(initialView);
	}

	/// <summary>
	/// A callback to be called when there has been a CameraMoveEvent
	/// </summary>
	/// <param name="newState"></param>
	private void OnCamMove(CameraMoveEvent.CameraState newState)
	{
		// Figure out which one is the correct camera
		CinemachineVirtualCamera newCam = null;
		switch (newState)
		{
			case CameraMoveEvent.CameraState.MainMenu:
				newCam = mainMenuCam;
				break;
			case CameraMoveEvent.CameraState.Packaging:
				newCam = packagingVCam;
				break;
			case CameraMoveEvent.CameraState.Shipping:
			default:
				newCam = shippingVCam;
				break;
		}
		// Set the correct cam priority
		if (lastCam) lastCam.Priority = 0;
		newCam.Priority = 1000;
		lastCam = newCam;
	}
}
