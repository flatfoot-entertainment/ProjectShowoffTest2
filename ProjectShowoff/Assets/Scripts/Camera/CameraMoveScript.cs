using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveScript : MonoBehaviour
{
	[SerializeField] private CameraMoveEvent.CameraState stateA;
	[SerializeField] private CameraMoveEvent.CameraState stateB;
	[SerializeField] private CameraMoveEvent.CameraState stateC;
	private CameraMoveEvent.CameraState current;
	// Start is called before the first frame update
	void Start()
	{
		EventScript.Handler.Subscribe(EventType.CameraMove, (e) =>
		{
			OnCamMove(((CameraMoveEvent)e).NewState);
		});
		current = stateA;
	}

	/// <summary>
	/// Toggle the camera state between <see cref="stateA"/> and <see cref="stateB"/>.
	/// </summary>
	public void Toggle()
	{
		current = current == stateA ? stateB : stateA;
		EventScript.Handler.BroadcastEvent(new CameraMoveEvent(current));
	}

	/// <summary>
	/// Switch the camera to the specified state.
	/// <br/>
	/// <c>0 => CameraState.Packaging</c>
	/// <br/>
	/// <c>1 => CameraState.Shipping</c>
	/// <br/>
	/// <c>2 => CameraState.MainMenu</c>
	/// </summary>
	public void SwitchCamera(int state)
	{
		switch (state)
		{
			case 0:
				current = CameraMoveEvent.CameraState.Packaging;
				break;
			case 1:
				current = CameraMoveEvent.CameraState.Shipping;
				break;
			case 2:
				current = CameraMoveEvent.CameraState.MainMenu;
				break;
		}
		EventScript.Handler.BroadcastEvent(new CameraMoveEvent(current));
	}

	/// <summary>
	/// A callback to be called when the Camera has moved
	/// </summary>
	private void OnCamMove(CameraMoveEvent.CameraState newState)
	{
		if (current == stateA)
		{
			if (newState == stateB) current = stateB;
		}
		else if (current == stateB)
		{
			if (newState == stateA) current = stateA;
		}
		else
		{
			if (newState == stateC) current = stateC;
		}
	}
}
