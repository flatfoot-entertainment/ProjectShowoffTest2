using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraState : MonoBehaviour
{
	[SerializeField] private CameraMoveEvent.CameraState targetState;
	CameraMoveEvent.CameraState current;
	CameraMoveEvent.CameraState last;
	// Start is called before the first frame update
	void Start()
	{
		EventScript.Handler.Subscribe(EventType.CameraMove, (e) =>
		{
			OnCamMove(((CameraMoveEvent)e).NewState);
		});
	}

	/// <summary>
	/// Move to the target state or move back to the last state before the target state
	/// </summary>
	public void Toggle()
	{
		if (current != targetState)
		{
			last = current;
			current = targetState;
			EventScript.Handler.BroadcastEvent(new CameraMoveEvent(current));
		}
		else
		{
			current = last;
			EventScript.Handler.BroadcastEvent(new CameraMoveEvent(current));
		}
	}

	private void OnCamMove(CameraMoveEvent.CameraState newState)
	{
		current = newState;
	}
}
