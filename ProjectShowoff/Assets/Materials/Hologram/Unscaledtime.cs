using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unscaledtime : MonoBehaviour
{
    private Renderer ren;

    private void Start()
    {
        ren = GetComponent<Renderer>();
    }

    private void Update()
    {
        ren.sharedMaterial.SetFloat("_UnscaledTime", Time.unscaledTime);
    }
}
